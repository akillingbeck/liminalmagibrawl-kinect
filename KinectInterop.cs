/************************************************************************
*                                                                       *
*   KinectInterop.cs -- All struct and enum data info here was taken    *
*        from http://msdn.microsoft.com/en-us/library/hh855366          *
*       and also the header files present in the Kinect SDK directories *
*   Author -- Andrew Killingbeck ,CLOCKinROCK games                     *
*                                                                       *
************************************************************************/
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System;
using System.Diagnostics;

namespace KinectWrapper
{


    #region INITIALISE
    //akill08 - FlagsAttribute indicate these values can be treated as a set of bit flags
    // see http://msdn.microsoft.com/en-us/library/hh855368#NUI_INITIALIZE
    [Flags]
    public enum KinectInitialiseFlags
    {
        // akill08 - values taken from NuiApi.H (The Includes from SDK folder)
        // C:\Program Files\Microsoft SDKs\Kinect\v1.5\inc\NuiApi.h
        UsesDepthAndPlayerIndex = 0x00000001,
        UsesColor = 0x00000002,
        UsesSkeleton = 0x00000008,
        UsesDepth = 0x00000020
    }
    #endregion

    #region SKELETAL INFO
    /// <summary>
    /// akill08 - Constants available at : http://msdn.microsoft.com/en-us/library/hh855368
    /// </summary>
    public static class SkeletonConstants
    {
        public static int SkeletonCount         = 6;
        public static int MaxSkeletonTracked    = 2;
        // akill08 - see http://msdn.microsoft.com/en-us/library/hh855368 for list of constants
    }

  
    public struct SkeletonFrame
    {
        public Int64 timeStamp;
        public uint frameNumber;
        public uint flags;
        public Vector4 floorClipPlane;
        public Vector4 normalToGravity;
        [MarshalAs(UnmanagedType.ByValArray,SizeConst=6,ArraySubType=UnmanagedType.Struct)]
        public SkeletonData[] skeletonData;


    }
    public struct SkeletonData
    {
        public SkeletonTrackingState trackingState;
        public uint trackingID;
        public uint enrollmentIndex;
        public uint userIndex;
        public Vector4 position;
        /// <summary>
        /// Marshal as an array with value of 20
        /// //TODO: is this right?
        /// Equivalent to : Vector4[] skeletonPositions = new Vector4[20];.....??
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray,SizeConst=20,ArraySubType=UnmanagedType.Struct)]
        public Vector4[] skeletonPositions;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20, ArraySubType = UnmanagedType.Struct)]
        public JointTrackingState[] jointTrackingState;
        public uint qualityFlags;
    }
    [Flags]
    public enum ImageStreamFlags
    {
        PlayerIndexShift = 3,
        PlayerIndexMask = ((1 << PlayerIndexShift) - 1),
        DepthMaximum = ((4000 << PlayerIndexShift) | PlayerIndexMask),
        DepthMinimum = (800 << PlayerIndexShift),
        DepthNoValue = 0,
        DistinctOverflowDepthValues = 0x00040000,
        EnableNearMode = 0x00020000,
        SuppressNoFrameData = 0x00010000,
        TooFarIsNonZero = 0x0004000,
        FrameLimitMaximum = 4
    }
    public enum ImageResolution
    {
        Invalid = -1,
        res80x60 = 0,
        res320x240,
        res640x480,
        res1280x960
    }
   // [Flags]
    public enum ImageTypeFlags
    {
        depthPlayerIndex = 0,
        colour,
        color_yuv,
        color_raw_yuv,
        depth
    }
    // Flags for use with the SkeletonTrackingEnable function
    // taken from SDK header files
    [Flags]
    public enum SkeletonTrackingFlags
    {
            SuppressNoFrameData         = 0x00000001,
            TitleSetsTrackedSkeletons   =0x00000002,
            EnableSeatedSupport         =0x00000004, 
            EnableInNearRange           =0x00000008
    }

    [StructLayoutAttribute(LayoutKind.Sequential)]
    public struct ImageFrame
    {
        public Int64 timeStamp;
        public uint frameNumber;
        public ImageTypeFlags imageType;
        public ImageResolution imageRes;
		
        public IntPtr ImageFrameTexture;

        public uint frameFlags;
        public _NUI_IMAGE_VIEW_AREA viewArea;


    }
    //not used
    public enum _NUI_IMAGE_DIGITALZOOM
    {
        NUI_IMAGE_DIGITAL_ZOOM_1X = 0
    }
    //not used
    public struct _NUI_IMAGE_VIEW_AREA
    {
        _NUI_IMAGE_DIGITALZOOM eDigitalZoom;
        long lCenterX;
        long lCenterY;
    };


    /// <summary>
    /// Maybe not needed - see : http://msdn.microsoft.com/en-us/library/nuisensor.nui_skeleton_data
    /// Values taken from NuiSkeleton.h - see Microsoft SDK include folder
    /// </summary>
    public enum SkeletonQualityFlags
    {
        /// <summary>
        /// Part of the player's body is out of frame to the camera's right.
        /// </summary>
        ClippedRight =  0x00000001,
        /// <summary>
        /// Part of the player's body is out of frame to the camera's left.
        /// </summary>
        ClippedLeft =   0x00000002,
        /// <summary>
        /// Part of the player's body is out of frame above the camera's field of view.
        /// </summary>
        ClippedTop =     0x00000004,
        /// <summary>
        /// Part of the player's body is out of frame below the camera's field of view.
        /// </summary>
        ClippedBottom =  0x00000008
    }
  
    /// <summary>
    /// akill08 - http://msdn.microsoft.com/en-us/library/nuisensor.nui_skeleton_position_tracking_state
    /// </summary>
    public enum SkeletonTrackingState
    {
        /// <summary>
        /// This means the Skeleton is not being tracked
        /// </summary>
        NotTracked = 0,
        /// <summary>
        /// The skeleton is being tracked, but we only know the general position, and
        /// we do not know the specific joint locations.
        /// </summary>
        PositionOnly,
        /// <summary>
        /// The skeleton is being tracked and the joint data is available for consumption.
        /// </summary>
        Tracked
    }

  /// <summary>
    ///  This struct is used to setup the skeleton smoothing values.
  /// </summary>
    public struct TransformSmoothParameters
    {
        /// <summary>
        ///  Gets or sets the correction value.
        /// </summary>
        public float Correction
        {
            get;
            set;
        }
       /// <summary>
        /// Gets or sets the jitter radius value.
       /// </summary>
        public float JitterRadius
        {
            get;
            set;
        }
      /// <summary>
        ///  Gets or sets the maximum deviation radius value.
      /// </summary>
        public float MaxDeviationRadius
        {
            get;
            set;
        }
      /// <summary>
        /// Gets or sets the prediction value.
      /// </summary>
        public float Prediction
        {
            get;
            set;
        }
      /// <summary>
        ///  Gets or sets the smoothing value.
      /// </summary>
        public float Smoothing
        {
            get;
            set;
        }
    }
    #endregion

    #region BONES
    public struct SkeletonBoneRotation
    {
        public Matrix4x4 rotationMatrix;
        public Vector4 rotationQuaternion;
    }
    public struct SkeletonBoneOrientation
    {

        public JointType endJoint;
        public JointType startJoint;
        public SkeletonBoneRotation hierarchicalRotation;
        public SkeletonBoneRotation absoluteRotation;
       
    }
    #endregion

    #region JOINTS
    /// <summary>
    /// akill08 - Available Joints for Skeletal Tracking
    /// </summary>
    public enum JointType
    {
        /// <summary>
        ///     The center of the hip.
        HipCenter = 0,
        /// </summary>
        /// <summary>
        //     The bottom of the spine.
        Spine = 1,
        /// <summary>
        /// The center of the shoulders.
        /// </summary>
        ShoulderCenter = 2,
        /// <summary>
        ///     The players head.
        ///     </summary>
        Head = 3,
        /// <summary>
        ///     The left shoulder.
        ///     </summary>
        ShoulderLeft = 4,
        /// <summary>
        ///     The left elbow.
        ///     </summary>
        ElbowLeft = 5,
        /// <summary>
        ///     The left wrist.
        ///     </summary>
        WristLeft = 6,
        /// <summary>
        ///     The left hand.
        ///     </summary>
        HandLeft = 7,
        /// <summary>
        ///    The right shoulder.
        ///    </summary>
        ShoulderRight = 8,
        /// <summary>
        ///     The right elbow.
        ///     </summary>
        ElbowRight = 9,
        /// <summary>
        ///     The right wrist.
        ///     </summary>
        WristRight = 10,
        /// <summary>
        ///     The right hand.
        ///     </summary>
        HandRight = 11,
        /// <summary>
        ///     The left hip.
        ///     </summary>
        HipLeft = 12,
        /// <summary>
        ///     The left knee.
        ///     </summary>
        KneeLeft = 13,
        //
        /// <summary>
        ///     The left ankle.
        ///     </summary>
        AnkleLeft = 14,
        /// <summary>
        ///     The left foot.
        ///     </summary>
        FootLeft = 15,
        /// <summary>
        ///     The right hip.
        ///     </summary>
        HipRight = 16,
        /// <summary>
        ///     The right knee.
        ///     </summary>
        KneeRight = 17,
        /// <summary>
        ///     The right ankle.
        ///     </summary>
        AnkleRight = 18,
        /// <summary>
        ///     The right foot.
        ///     </summary>
        FootRight = 19,
        /// <summary>
        /// The total number of joints
        /// </summary>
        Count = 20
    }
    #endregion
	
	#region BODY_VECTORS
	// ngs - main vectors of the body
	public enum BodyVector
	{
		Head_ShoulderCenter = 0,
		ShoulderCenter_ShoulderRight = 1,
		ShoulderRight_ElbowRight = 2,
		ElbowRight_WristRight = 3,
		WristRight_HandRight = 4,
		ShoulderCenter_ShoulderLeft = 5,
		ShoulderLeft_ElbowLeft = 6,
		ElbowLeft_WristLeft = 7,
		WristLeft_HandLeft = 8,
		ShoulderCenter_Spine = 9,
		Spine_HipCenter = 10,
		HipCenter_HipRight = 11,
		HipRight_KneeRight = 12,
		KneeRight_AnkleRight = 13,
		AnkleRight_FootRight = 14,
		HipCenter_HipLeft = 15,
		HipLeft_KneeLeft = 16,
		KneeLeft_AnkleLeft = 17,
		AnkleLeft_FootLeft= 18,
        ShoulderCenter_HipCenter = 19,
        ShoulderLeft_WristLeft = 20,
		Count = 21
	}
    #endregion

    #region BODY_ANGLES
    public enum BodyAngle
    {
        elbowRight = 0,
        elbowLeft = 1,
        armpitRight = 2,
        armpitLeft = 3,
        Count = 4
    }
    #endregion

    /// <summary>
    /// akill08 - http://msdn.microsoft.com/en-us/library/nuisensor.nui_skeleton_position_tracking_state
    /// </summary>
    public enum JointTrackingState
    {
        /// <summary>
        ///  The joint is not tracked and no data is known about this joint.
        /// </summary>
        NotTracked = 0,
        /// <summary>
        ///  The joint location is inferred. The data should be used with the understanding
        ///  that the confidence level of the location is very low.
        /// </summary>
        Inferred,
        /// <summary>
        /// The joint is tracked and the data can be trusted.
        /// </summary>
        Tracked
    }
	
	//typedef struct _NUI_LOCKED_RECT {
   // INT Pitch;
   // int size;
   // BYTE *pBits;
//} 
	
	//typedef struct _NUI_SURFACE_DESC {
   // UINT Width;
    //UINT Height;
//} 
	[StructLayout(LayoutKind.Sequential)]
	public struct SurfaceDesc
	{
		uint Width;
		uint Height;
	}
	[StructLayout(LayoutKind.Sequential)]
	public struct LockedRect
	{
		Int32 Pitch;
		int size;
		[MarshalAs(UnmanagedType.LPArray)]
		byte[] pBits;
	}

    [StructLayout(LayoutKind.Sequential)]
	public struct ColorCust
	{
		public byte b;
		public byte g;
		public byte r;
		public byte a;
	}
	[StructLayout(LayoutKind.Sequential)]
	public struct ColorBuffer
	{
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1280 * 960, ArraySubType = UnmanagedType.Struct)]
		public ColorCust[] pixels;
	}
	
	public struct ColorFromPtr
	{
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1280 * 960)]
		public byte[] colour;
	}

    [StructLayout(LayoutKind.Sequential)]
	public class ImageBuffer
	{
	
		public uint randomVar1;
		
		public uint randomVar2;
		
		//[MarshalAs(UnmanagedType.ByValArray,SizeConst = 640 * 480)]
		public uint randomVar3;
		
		public uint randomVar4;
		public uint randomVar5;
		
		public uint width;
		public uint height;
		
		
		public uint randomVar6;
		public IntPtr pBuffer;
		//[MarshalAs(UnmanagedType.ByValArray, SizeConst = 640 * 480)]
		//public byte[] src;
		//public uint width;
		
		
		//[MarshalAs(UnmanagedType.ByValArray, SizeConst = 640 * 480)]
		//public byte[] buffer;
		//[MarshalAs(UnmanagedType.ByValArray, SizeConst = 640 * 480)]
		//public byte[] buffer;
		
		//public IntPtr bufff;
		//[MarshalAs(UnmanagedType.LPArray)]
		//public byte[] width;
		//public uint width;
		//[MarshalAs(UnmanagedType.ByValArray, SizeConst = 640 * 480)]
		//public byte[] dest;
		//[MarshalAs(UnmanagedType.ByValArray, SizeConst = 640 * 480)]
		//	public byte[] src;
		//public IntPtr buffer;
		//public Int32 buffer;
		////[MarshalAs(UnmanagedType.ByValArray, SizeConst = 640 * 480)]
		//public IntPtr dest;
		//
		////[MarshalAs(UnmanagedType.ByValArray, SizeConst = 640 * 480)]
		//public IntPtr src;
		//
		//public IntPtr size;
		//public IntPtr buffer;
		//[MarshalAs(UnmanagedType.ByValArray, SizeConst = 640 * 480)]
		//public byte[] dest;
		//[MarshalAs(UnmanagedType.ByValArray, SizeConst = 640 * 480)]
		//public byte[] src;
		//
		//public IntPtr size;
		
	}
	
	public struct NuiStatusProc
		{
		}

    [ComImport]
    [Guid("13ea17f5-ff2e-4670-9ee5-1297a6e880d1"),
    InterfaceType(ComInterfaceType.InterfaceIsIUnknown)] 
    interface INuiFrameTexture
    {
       
        [PreserveSig]
        int BufferLen();
		[PreserveSig]
        int Pitch();
		[PreserveSig]
       	Int32 LockRect(uint Level,ref LockedRect lockedRect,ref Rect rect,uint flags);
		[PreserveSig]
		Int32 GetLevelDesc(uint level,ref SurfaceDesc surfDesc);
		[PreserveSig]
		Int32 UnlockRect();
    }
    /// <summary>
    /// akill08 - The class containing methods that were imported from outside .dll's
    /// </summary>
    public class Kinect
    {

        //see http://msdn.microsoft.com/en-us/library/hh855370 for list of methods part of the API
        #region General Functions
        /// <summary>
        /// Initializes the sensor. If the sensor is already initialized, this will shut down the sensor
        /// and reinitialize it.
        /// </summary>
        /// <param name="flags">
        /// The NUI subsystems to initialize, as a bitwise-OR combination of the KinectInitialiseFlags constants.
        /// </param>
        [DllImport(@"C:\Windows\System32\Kinect10.dll", EntryPoint = "NuiInitialize")]
        public static extern int Initialize(KinectInitialiseFlags flags);

        /// <summary>
        /// Shuts down the sensor. If the sensor is already shut down, nothing happens.
        /// </summary>
        [DllImport(@"C:\Windows\System32\Kinect10.dll",EntryPoint="NuiShutdown")]
        public static extern void Shutdown();

        /// <summary>
        /// Sets the elevation angle of the Kinect sensor.
        /// </summary>
        /// <param name="angle">
        /// The elevation angle relative to gravity, in degrees. A value of zero indicates that the sensor
        /// array should point exactly horizontally. Positive values indicate that the sensor array should
        /// point above the horizon, and negative values indicate that the sensor array should point below
        /// the horizon. This value is constrained between -27 and
        /// 27.
        /// </param>
        [DllImport(@"C:\Windows\System32\Kinect10.dll", EntryPoint = "NuiCameraElevationSetAngle")]
        public static extern int SetCameraAngle(long angle);
        /// <summary>
        /// Gets the elevation angle of the Kinect sensor.
        /// </summary>
        /// <param name="angle">Reference to a LONG which receives the angle of the sensor in degrees.</param>
        [DllImport(@"C:\Windows\System32\Kinect10.dll", EntryPoint = "NuiCameraElevationGetAngle")]
        public static extern int GetCameraAngle(ref long angle);
        #endregion

        #region Skeleton Functions
        [DllImport(@"C:\Windows\System32\Kinect10.dll", EntryPoint = "NuiTransformSmooth")]
        public static extern int TransformSmooth(ref SkeletonFrame skeletonFrame, ref TransformSmoothParameters transformSmoothParams);
        /// <summary>
        /// Gets the next frame of data from the skeleton stream.
        /// </summary>
        /// <param name="millisecondsToWait">The timeout (in milliseconds) before returning without a new frame.</param>
        /// <param name="skeletonFrame">A pointer to a SkeletonFrame structure that receives the next image frame in the skeleton
        /// stream. This must not be NULL.</param>
        /// <returns></returns>
        [DllImport(@"C:\Windows\System32\Kinect10.dll", EntryPoint = "NuiSkeletonGetNextFrame")]
        public static extern int GetNextSkeletonFrame(uint millisecondsToWait, ref SkeletonFrame skeletonFrame);
        
        /// <summary>
        /// Disables skeleton tracking.
        /// </summary>
         [DllImport(@"C:\Windows\System32\Kinect10.dll", EntryPoint = "NuiSkeletonTrackingDisable")]
        public static extern int DisableSkeletonTracking();

         /// <summary>
         /// Enables skeleton tracking.
         /// </summary>
         /// <param name="nextFrameEvent">
         /// A handle to an application-allocated, manual reset event that will be set whenever a new frame
         /// of skeleton data is available, and will be reset whenever the latest frame data is returned.
         /// This can be NULL.
         /// </param>
         /// <param name="flags">
         /// Flags that control skeleton tracking, as a bitwise-OR combination SkeletonTrackingFlags values.
         /// </param>
         [DllImport(@"C:\Windows\System32\Kinect10.dll", EntryPoint = "NuiSkeletonTrackingEnable")]
         public static extern int EnableSkeletonTracking(IntPtr nextFrameEvent,SkeletonTrackingFlags flags);
         /// <summary>
         /// Calculate bone orientations for a skeleton.
         /// </summary>
         /// <param name="skeletonData">
         /// Pointer to the skeleton data to calculate joint angles for.
         /// </param>
         /// <param name="boneOrientations">
         /// Pointer to an array of SkeletonBoneOrientation of dimension  (20).
         /// This array must be allocated by the user before calling this function.
         /// </param>
         [DllImport(@"C:\Windows\System32\Kinect10.dll", EntryPoint = "NuiSkeletonCalculateBoneOrientations")]
         public static extern int CalculateBoneOrientations(ref SkeletonData skeletonData, SkeletonBoneOrientation[] boneOrientations);


         [DllImport(@"C:\Windows\System32\Kinect10.dll", EntryPoint = "NuiImageStreamOpen")]
         public static extern int OpenImageStream(ImageTypeFlags imageType,ImageResolution imageRes,ImageStreamFlags streamFlags,
             uint frameLimit,IntPtr nextFrameEvent,ref IntPtr openedStream);

         [DllImport(@"C:\Windows\System32\Kinect10.dll", EntryPoint = "NuiImageStreamGetNextFrame")]
         public static extern int GetNextImageFrame(IntPtr imageStream, uint millisecondsToWait,ref IntPtr imageFrame);

         [DllImport(@"C:\Windows\System32\Kinect10.dll", EntryPoint = "NuiImageStreamReleaseFrame")]
         public static extern int ReleaseImageFrame(IntPtr openedStream, IntPtr frame);

         [DllImport(@"C:\Windows\System32\Kinect10.dll", EntryPoint = "NuiImageResolutionToSize")]
         public static extern void ImageResToSize(ImageResolution res, ref uint width,ref uint height);     
        #endregion
		
		[DllImport(@"C:\Windows\System32\Kinect10.dll", EntryPoint = "NuiSetDeviceStatusCallback")]
	    public static extern void NuiSetDeviceStatusCallback(NuiStatusProc callback);

    }
}