/*
 * KinectModelController.cs - Handles rotating the bones of a model to match 
 * 			rotations derived from the bone positions given by the kinect
 * 
 * 		Developed by Peter Kinney -- 6/30/2011
 */

using UnityEngine;
using System;
using System.Collections;
using KinectWrapper;

public class KinectModelController : MonoBehaviour
{
    float difference;
    //Assignments for a bitmask to control which bones to look at and which to ignore
    public enum BoneMask
    {
        None = 0x0,
        //EMPTY = 0x1,
        Spine = 0x2,
        Shoulder_Center = 0x4,
        Head = 0x8,
        Shoulder_Left = 0x10,
        Elbow_Left = 0x20,
        Wrist_Left = 0x40,
        Hand_Left = 0x80,
        Shoulder_Right = 0x100,
        Elbow_Right = 0x200,
        Wrist_Right = 0x400,
        Hand_Right = 0x800,
        Hips = 0x1000,
        Knee_Left = 0x2000,
        Ankle_Left = 0x4000,
        Foot_Left = 0x8000,
        //EMPTY = 0x10000,
        Knee_Right = 0x20000,
        Ankle_Right = 0x40000,
        Foot_Right = 0x80000,
        All = 0xEFFFE,
        Torso = 0x1000000 | Spine | Shoulder_Center | Head, //the leading bit is used to force the ordering in the editor
        Left_Arm = 0x1000000 | Shoulder_Left | Elbow_Left | Wrist_Left | Hand_Left,
        Right_Arm = 0x1000000 | Shoulder_Right | Elbow_Right | Wrist_Right | Hand_Right,
        Left_Leg = 0x1000000 | Hips | Knee_Left | Ankle_Left | Foot_Left,
        Right_Leg = 0x1000000 | Hips | Knee_Right | Ankle_Right | Foot_Right,
        R_Arm_Chest = Right_Arm | Spine,
        No_Feet = All & ~(Ankle_Left | Ankle_Right),
        Upper_Body = Torso | Left_Arm | Right_Arm
    }

    public KinectSkeletonWrapper sw;

    public GameObject Hip_Center;
    public GameObject Spine;
    public GameObject Shoulder_Center;
    public GameObject Head;
    public GameObject Collar_Left;
    public GameObject Shoulder_Left;
    public GameObject Elbow_Left;
    public GameObject Wrist_Left;
    public GameObject Hand_Left;
    public GameObject Fingers_Left; //unused
    public GameObject Collar_Right;
    public GameObject Shoulder_Right;
    public GameObject Elbow_Right;
    public GameObject Wrist_Right;
    public GameObject Hand_Right;
    public GameObject Fingers_Right; //unused
    public GameObject Hip_Override;
    public GameObject Hip_Left;
    public GameObject Knee_Left;
    public GameObject Ankle_Left;
    public GameObject Foot_Left;
    public GameObject Hip_Right;
    public GameObject Knee_Right;
    public GameObject Ankle_Right;
    public GameObject Foot_Right;

    private float previousXHip;
    private float previousYHip;

    public int player;
    public BoneMask Mask = BoneMask.All;
    public bool animated;
    public float blendWeight = 1;

    [HideInInspector]
    public GameObject[] _bones; //internal handle for the bones of the model
    private uint _nullMask = 0x0;

    private Quaternion[] _baseRotation; //starting orientation of the joints
    private Vector3[] _boneDir; //in the bone's local space, the direction of the bones
    private Vector3[] _boneUp; //in the bone's local space, the up vector of the bone
    private Vector3 _hipRight; //right vector of the hips
    private Vector3 _chestRight; //right vectory of the chest

    // ngs
    [HideInInspector]
    public Vector3 initHipPosition;
    public Vector3 currentPosition;
    private Vector3 currentVerticalPosition;
    private Vector3 hipPositionKinect;
    private Vector3 movementDirection;
    private Vector3 duckDirection;
    public float characterPlaceOffset = 0.5f;
    public float scaleMovement = 1.7f;


    public float initialSkeletonHipY = -1;
    private float hipXdisplaceMent;
    private float hipYdisplacement;
    private Vector3 verticalDisplacement;
    private float yDisplacement;

    private Vector3 previousAnklePosition;
    //these are used to reset to original, for the reset
    private float chr_placeOffset;
    private float chr_scaleMovement;

    #region MOVEMENT_LIMITATIONS
    // To stop going beyond the bridge borders
    public GameObject leftLimit;
    public GameObject rightLimit;
    public GameObject upLimit;
    public GameObject downLimit;

    public float lastPosInsideLimits;  // kinect x position
    public bool insideXLimits = false;
    public bool insideYLimits = false;
    #endregion

    #region PICTURES
    private float elpasedTime = 0; // to evoid taking pictures when people is getting ready
    public ColourImage kinectCamera;
    private bool pictureDuckingTaken = false;
    private bool pictureMovingSideTaken = false;

    [HideInInspector]
    public bool matchStarted = false;
    #endregion

    // Use this for initialization
    void Start()
    {
        //store bones in a list for easier access, everything except Hip_Center will be one
        //higher than the corresponding Kinect.NuiSkeletonPositionIndex (because of the hip_override)
        _bones = new GameObject[(int)JointType.Count + 5] {
			null, Hip_Center, Spine, Shoulder_Center,
			Collar_Left, Shoulder_Left, Elbow_Left, Wrist_Left,
			Collar_Right, Shoulder_Right, Elbow_Right, Wrist_Right,
			Hip_Override, Hip_Left, Knee_Left, Ankle_Left,
			null, Hip_Right, Knee_Right, Ankle_Right,
			//extra joints to determine the direction of some bones
			Head, Hand_Left, Hand_Right, Foot_Left, Foot_Right};

        //determine which bones are not available
        for (int ii = 0; ii < _bones.Length; ii++)
        {
            if (_bones[ii] == null)
            {
                _nullMask |= (uint)(1 << ii);
            }
        }

        //store the base rotations and bone directions (in bone-local space)
        _baseRotation = new Quaternion[(int)JointType.Count];
        _boneDir = new Vector3[(int)JointType.Count];

        //first save the special rotations for the hip and spine
        _hipRight = Hip_Right.transform.position - Hip_Left.transform.position;
        _hipRight = Hip_Override.transform.InverseTransformDirection(_hipRight);

        _chestRight = Shoulder_Right.transform.position - Shoulder_Left.transform.position;
        _chestRight = Spine.transform.InverseTransformDirection(_chestRight);

        //get direction of all other bones
        for (int ii = 0; ii < (int)JointType.Count; ii++)
        {
            if ((_nullMask & (uint)(1 << ii)) <= 0)
            {
                //save initial rotation
                _baseRotation[ii] = _bones[ii].transform.localRotation;

                //if the bone is the end of a limb, get direction from this bone to one of the extras (hand or foot).
                if (ii % 4 == 3 && ((_nullMask & (uint)(1 << (ii / 4) + (int)JointType.Count)) <= 0))
                {
                    _boneDir[ii] = _bones[(ii / 4) + (int)JointType.Count].transform.position - _bones[ii].transform.position;
                }
                //if the bone is the hip_override (at boneindex Hip_Left, get direction from average of left and right hips
                else if (ii == (int)JointType.HipLeft && Hip_Left != null && Hip_Right != null)
                {
                    _boneDir[ii] = ((Hip_Right.transform.position + Hip_Left.transform.position) / 2F) - Hip_Override.transform.position;
                }
                //otherwise, get the vector from this bone to the next.
                else if ((_nullMask & (uint)(1 << ii + 1)) <= 0)
                {
                    _boneDir[ii] = _bones[ii + 1].transform.position - _bones[ii].transform.position;
                }
                else
                {
                    continue;
                }
                //Since the spine of the kinect data is ~40 degrees back from the hip,
                //check what angle the spine is at and rotate the saved direction back to match the data
                if (ii == (int)JointType.Spine)
                {
                    float angle = Vector3.Angle(transform.up, _boneDir[ii]);
                    _boneDir[ii] = Quaternion.AngleAxis(-40 + angle, transform.right) * _boneDir[ii];
                }
                //transform the direction into local space.
                _boneDir[ii] = _bones[ii].transform.InverseTransformDirection(_boneDir[ii]);
            }
        }
        //make _chestRight orthogonal to the direction of the spine.
        _chestRight -= Vector3.Project(_chestRight, _boneDir[(int)JointType.Spine]);
        //make _hipRight orthogonal to the direction of the hip override
        Vector3.OrthoNormalize(ref _boneDir[(int)JointType.HipLeft], ref _hipRight);

        //ngs
        initHipPosition = Hip_Center.transform.position;
        movementDirection = transform.Find("bully" + (player + 1)).transform.right;
        duckDirection = transform.Find("bully" + (player + 1)).transform.up;
        hipPositionKinect = sw.jointPositions[(int)player, (int)JointType.HipCenter];

        chr_placeOffset = characterPlaceOffset;
        chr_scaleMovement = scaleMovement;
    }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    //void OnGUI()
    //{
    //    if (player == 0)
    //    {
    //        GUIStyle g = new GUIStyle();
    //        g.fontSize = 50;
    //        GUI.Label(new Rect(0, 0, 500, 300), "DIFFERENCE : " + difference, g);
    //        GUI.Label(new Rect(0, 100, 500, 300), "INITIAL Y : " + initialSkeletonHipY, g);
    //        GUI.Label(new Rect(0, 200, 500, 300), "Kinect Y : " + hipPositionKinect.y, g);
    //        GUI.Label(new Rect(0, 300, 500, 300), "COMBINED : " + (initialSkeletonHipY + difference), g);
           
    //    }
    //}

    void Update()
    {
        
        //update the data from the kinect if necessary
        if (sw.PollSkeleton())
        {

            if (Input.GetKeyDown(KeyCode.P))
            {
                ResetInitialModelPosition();
            }
            for (int ii = 0; ii < (int)JointType.Count; ii++)
            {
                if (((uint)Mask & (uint)(1 << ii)) > 0 && (_nullMask & (uint)(1 << ii)) <= 0)
                {
                    RotateJoint(ii);
                }
            }

            previousXHip = Hip_Center.transform.position.x;
            previousYHip = Hip_Center.transform.position.y;
            // ngs
            if (ScriptVsManager.introCameraFinished)
            {
                DisplacementSide();
                DisplaceUpDown();
            }
            hipPositionKinect = sw.jointPositions[(int)player, (int)JointType.HipCenter];

            if (insideXLimits)
            {
                Hip_Center.transform.position = new Vector3(hipXdisplaceMent, Hip_Center.transform.position.y, Hip_Center.transform.position.z);
            }
            else
            {
                Hip_Center.transform.position = new Vector3(previousXHip, Hip_Center.transform.position.y, Hip_Center.transform.position.z);
            }
            if (insideYLimits)
            {
                Hip_Center.transform.position = new Vector3(Hip_Center.transform.position.x, hipYdisplacement, Hip_Center.transform.position.z);
            }
            else
            {
                Hip_Center.transform.position = new Vector3(Hip_Center.transform.position.x, previousYHip, Hip_Center.transform.position.z);
            }

            previousAnklePosition = Ankle_Left.transform.position;
            //MOved this here so Both displacements can use it
            //if (insideLimits)
            //{
            //    Hip_Center.transform.position = currentPosition;
            //}
        }

        if(matchStarted)
        {
            elpasedTime += Time.deltaTime;
        }
    }
    void ResetInitialModelPosition()
    {
        Hip_Center.transform.position = initHipPosition;
    }
    void RotateJoint(int bone)
    {

        if ( bone == (int)JointType.FootLeft
                   || bone == (int)JointType.FootRight || bone == (int)JointType.AnkleLeft || bone == (int)JointType.AnkleRight)
        {
            return;
        }


        //if blendWeight is 0 there is no need to compute the rotations
        if (blendWeight <= 0)
        {
            return;
        }
        Vector3 upDir = new Vector3();
        Vector3 rightDir = new Vector3();
        if (bone == (int)JointType.Spine)
        {
            upDir = ((Hip_Left.transform.position + Hip_Right.transform.position) / 2F) - Hip_Override.transform.position;
            rightDir = Hip_Right.transform.position - Hip_Left.transform.position;
        }

        //if the model is not animated, reset rotations to fix twisted joints
        if (!animated)
        {
            _bones[bone].transform.localRotation = _baseRotation[bone];
        }

       
            //if (sw.jointTrackingStates[player, bone] == JointTrackingState.Inferred)
            //{
               
           // }

           
        
        //if the required bone data from the kinect isn't available, return
        if (sw.jointTrackingStates[player, bone] == JointTrackingState.NotTracked)
        {
            return;
        }
       

        //get the target direction of the bone in world space
        //for the majority of bone it's bone - 1 to bone, but Hip_Override and the outside
        //shoulders are determined differently.

        Vector3 dir = _boneDir[bone];
        Vector3 target;

        //if bone % 4 == 0 then it is either an outside shoulder or the hip override
        if (bone % 4 == 0)
        {
            //hip override is at Hip_Left
            if (bone == (int)JointType.HipLeft)
            {
                //target = vector from hip_center to average of hips left and right
                target = ((sw.jointPositions[player, (int)JointType.HipLeft] + sw.jointPositions[player, (int)JointType.HipRight]) / 2F) - sw.jointPositions[player, (int)JointType.HipCenter];
            }
            //otherwise it is one of the shoulders
            else
            {
                //target = vector from shoulder_center to bone
                target = sw.jointPositions[player, bone] - sw.jointPositions[player, (int)JointType.ShoulderCenter];
            }
        }
        else
        {
            //target = vector from previous bone to bone
            target = sw.jointPositions[player, bone] - sw.jointPositions[player, bone - 1];
        }
        //transform it into bone-local space (independant of the transform of the controller)
        target = transform.TransformDirection(target);
        target = _bones[bone].transform.InverseTransformDirection(target);
        //create a rotation that rotates dir into target
        Quaternion quat = Quaternion.FromToRotation(dir, target);
        //if bone is the spine, add in the rotation along the spine
        if (bone == (int)JointType.Spine)
        {
            //rotate the chest so that it faces forward (determined by the shoulders)
            dir = _chestRight;
            target = sw.jointPositions[player, (int)JointType.ShoulderRight] - sw.jointPositions[player, (int)JointType.ShoulderLeft];

            target = transform.TransformDirection(target);
            target = _bones[bone].transform.InverseTransformDirection(target);
            target -= Vector3.Project(target, _boneDir[bone]);

            quat *= Quaternion.FromToRotation(dir, target);

        }
        //if bone is the hip override, add in the rotation along the hips
        else if (bone == (int)JointType.HipLeft)
        {
            //rotate the hips so they face forward (determined by the hips)
            dir = _hipRight;
            target = sw.jointPositions[player, (int)JointType.HipRight] - sw.jointPositions[player, (int)JointType.HipLeft];

            target = transform.TransformDirection(target);
            target = _bones[bone].transform.InverseTransformDirection(target);
            target -= Vector3.Project(target, _boneDir[bone]);

            quat *= Quaternion.FromToRotation(dir, target);


        }

        //reduce the effect of the rotation using the blend parameter
        quat = Quaternion.Lerp(Quaternion.identity, quat, blendWeight);
        //apply the rotation to the local rotation of the bone
        _bones[bone].transform.localRotation = _bones[bone].transform.localRotation * quat;

        if (bone == (int)JointType.Spine)
        {
            restoreBone(_bones[(int)JointType.HipLeft], _boneDir[(int)JointType.HipLeft], upDir);
            restoreBone(_bones[(int)JointType.HipLeft], _hipRight, rightDir);
        }

        return;
    }

    void restoreBone(GameObject bone, Vector3 dir, Vector3 target)
    {
        //transform target into bone-local space (independant of the transform of the controller)
        //target = transform.TransformDirection(target);
        target = bone.transform.InverseTransformDirection(target);
        //create a rotation that rotates dir into target
        Quaternion quat = Quaternion.FromToRotation(dir, target);
        bone.transform.localRotation *= quat;
    }

    // ngs - move character sidewards
    void DisplacementSide()
    {
        if (player == 0)
        {
            currentPosition = initHipPosition
                              + movementDirection * hipPositionKinect.x * scaleMovement
                              + movementDirection * characterPlaceOffset;
           
        }
        else
        {
            currentPosition = initHipPosition
                              + movementDirection * hipPositionKinect.x * scaleMovement
                              - movementDirection * characterPlaceOffset;
        }
        hipXdisplaceMent = currentPosition.x;

        limitMovementSide();
    }

    void limitMovementSide()
    {
        if ((transform.TransformPoint(currentPosition).x < transform.TransformPoint(rightLimit.transform.position).x)
            && (transform.TransformPoint(currentPosition).x > transform.TransformPoint(leftLimit.transform.position).x))
        {
            insideXLimits = true;
        }
        else
        {
            // save last position he/she was on the bridge
            if (insideXLimits)
            {
                lastPosInsideLimits = Hip_Center.transform.position.x;
            }

            insideXLimits = false;

            // take picture when moving a lot to the side
            if (!pictureMovingSideTaken && elpasedTime > 10)
            {
                kinectCamera.TakePicture();
                pictureMovingSideTaken = true;
            }
        }
    }

    void DisplaceUpDown()
    {
        if (initialSkeletonHipY != -1)
        {
            difference = Math.Abs(initialSkeletonHipY - hipPositionKinect.y);
            currentVerticalPosition = initHipPosition
                                 + duckDirection * (((initialSkeletonHipY - difference) - initialSkeletonHipY)) * 5f;

            hipYdisplacement = currentVerticalPosition.y;

            limitVerticalMovements();
        }
    }
    void limitVerticalMovements()
    {
        if ((transform.TransformPoint(currentVerticalPosition)).y > transform.TransformPoint(downLimit.transform.position).y
            && (transform.TransformPoint(currentVerticalPosition).y < transform.TransformPoint(upLimit.transform.position).y))
        {
            insideYLimits = true;
        }
        else
        {
            insideYLimits = false;

            // take picture when ducking
            if (!pictureDuckingTaken && elpasedTime > 5
                && transform.TransformPoint(currentVerticalPosition).y < transform.TransformPoint(downLimit.transform.position).y)
            {
                kinectCamera.TakePicture();
                pictureDuckingTaken = true;
            }
        }
    }


    public void notDisplacement()
    {
       // initialSkeletonHipY = -1;
        characterPlaceOffset = 0;
        scaleMovement = 0;
    }

    public void ResetKinectMovement()
    {
        //
        characterPlaceOffset = chr_placeOffset;
        scaleMovement = chr_scaleMovement;
    }

    public void setInitialSkeletonHipY(float yValue)
    {
        Hip_Center.transform.position = initHipPosition;
        initialSkeletonHipY = yValue;
        resetPictureVariables();
    }

    public void resetPictureVariables()
    {
        elpasedTime = 0;
        pictureDuckingTaken = false;
        pictureMovingSideTaken = false;
        matchStarted = false;
    }
}