using UnityEngine;
using System.Collections;
using KinectWrapper;

public class GestureManager : MonoBehaviour
{


    public GameObject moveBack;
    #region ATTRIBUTES
    public float minZDistance = 2.0f;
    private bool zDistance = false;
    #region ENUMS
    public enum DefenseStance
    {
        RightFootFront = 0,
        LeftFootFront = 1,
    }

    public enum HandChoices
    {
        Left,
        Right

    }

    public enum SpecialAttackPoses
    {
        crossArms = 0,
        armsBehindBack = 1,
        armsForward = 2
    }
    #endregion

    #region PLAYER_VARIABLES
    private PlayerIndex playerIndex;
    public KinectSkeletonWrapper skeleton;
    private ScriptPlayer scriptPlayer;
    #endregion

    #region ATTACK_MODE
    private AttackMode attackMode = AttackMode.attack;
    private AttackMode attackModeOld; //previous attackMode
    private bool attackModeChanged = false;

    // max separation of your feet before leave the neutral mode
    private float distanceDefenseMode = 0.2f;
    #endregion

    #region PREFERENCES
    private DefenseStance stance = DefenseStance.RightFootFront;
    public HandChoices aimingHandChoice;
    public HandChoices shieldArmChoice;

    private JointType attackChargeHand;
    private JointType defenseChargeHand;
    private JointType aimingHand;
    private JointType shieldingShoulder;
    private JointType chargingHand;
    private JointType shootingHand;
    private JointType aimingArmElbow;
    private JointType frontFoot;
    private JointType backFoot;
    private JointType shieldElbow;
    private BodyAngle shieldArmElbow;
    private BodyAngle shieldArmPit;
    #endregion

    #region BODY_ACTIVITY_VARIABLES
    // The minimum average speed of all joints to determine whether or not the special bar should increase
    public float minActiveSpeed = 40;
    #endregion

    #region RECHARGE VARIABLES
    private bool chr_playerIsCharging = false;

    private float chr_timeResetChargeTimer = 1.0f;
    private float chr_timeChargingNext = 0;

    // charge got each time you shake the hand up or down
    private float chr_amountRecharge = 1.0f;
    private float chr_maxAmountRecharge = 3.0f;
    private float chr_initAmountRecharge = 1.0f;
    private float chr_amountIncreaseByShake = 0.05f;

    private int chr_numberShakes = 0;                       // number of shakes done while charging
    private bool chr_upShake = true;          // direction you charged last time (up or down)

    [HideInInspector]
    public float chr_Power = 100;           // it could be whatever value. This is not the energy of the player
    private float chr_prevPower;

    private float chr_minShakeSpeed = 4.0f;          // m/s
    private float chr_timePowerDown = 0.2f;          // time it will take to decresae one shake when you are using the shield
    private float chr_timeDecreasingPower = 0;
    #endregion

    #region AIMING_VARIABLES
    public bool aiming = false;
    public bool aimStop = false;            // If true stop aiming 
    public float aimArmDistance = 0.1f;     // Distance that your arm must be ahead you (meters)
    #endregion

    #region SHOOTING_VARIABLES
    private float sht_MinSpeedStart = 1.2f;       // speed you must reach to start counting as a shooting gesture

    // to check availability of the shoot
    private bool sht_Available = true;
    private bool sht_HandCameBack = true;

    // reset shoot either pulling back your hand certain distance or coming back quickly
    private float sht_DistanceComeBack = 0.08f;
    private float sht_ComeBackSpeed = 1.2f;

    // time you have to wait to shoot again (small value to avoid bugs like shooting several times at once)
    private float sht_TimeNext = 0.05f;
    private float sht_TimeWaiting = 0;

    private float sht_Distance = 0.1f;          // Distance to cover forward before shoot.
    private float sht_Time = 4;                // Time in which distance to shoot must be covered

    private float sht_Threshold = 0.05f;       // amount your hand can go backwards without cancel the gesture. To avoid the fluctuations of the kinect tracking
    private Vector3 sht_initPos;               // init position of shooting hand when the gesture starts.
    private Vector3 sht_endPos;
    private Vector3 sht_GreatestPos;            // farthest position that your shooting hand reach while doing the gesture
    private float sht_TimePassed;
    private bool sht_GestureStarted = false;

    private float sht_minSpeedLevelUp = 3;
    private bool sht_nextLevelAvailable = true; //obsolete variable

    private float sht_angleChangeDirection = 25;
    private float sht_timeCheckDirectionAfterShoot = 0.1f;
    private float sht_timerAfterShoot = 0;
    private Vector3 sht_posDirection; // pos calculated after shoot to decide shoot direction
    private bool posDirectionGot = false;
    #endregion

    #region BALL_ENERGY_VARIABLES
    private bool be_gatheringEnergy = false;
    private float be_timeNextLevel = 1f;
    private float be_timeGathereingEnergy = 0;
    #endregion

    #region SWITCH_MAGIC_VARIABLES
    public float switchMagicSpeed = 4.0f; //(meters/seconds)
    #endregion

    #region SIMPLE_SHIELD_VARIABLES
    // public float shieldDiference      = 0.1f;  // Difference allowed to have between your elbow and you wrist. How parallel your arm must be
    //public float shieldElbowDistance  = 0.2f;    // Distance you must put far your elbow
    private float shieldMaxAngleElbow = 130;     // Max angle your elbow can have to allow shield defense 
    private float shieldMinAngleArmpit = 35;
    private bool shieldGestureDone = false;
    #endregion

    #region MAGIC_SHIELD_VARIABLES
    private bool ms_activated = false;
    private bool ms_charging = false;
    private bool ms_waitingForActivation = false;

    private float ms_timeCharging = 0;  // time arms up
    private float ms_timeActivated = 0;

    public float ms_timeToActivate = 2; // time after charging it to make the final gesture that activates the shield
    private float ms_timeWaitingActivation = 0;

    private float ms_forwardDistance = 0.2f;

    private float ms_multiplier = 2;
    #endregion

    #region SPECIAL ATTACK VARIABLES
    private bool[] specialMoveCombo = new bool[3];

    private float spc_timeout = 10f;    // time for each gesture of the combo
    private float spc_gestureTimer = 0;
    private bool spc_startTimer = false;
    private float spc_armsCrossedDist = 0.05f;   // distance elbows ahead while crossing arms
    private float spc_armsBackDist = 0.05f;
    private float spc_forwardDist = 0.1f;
    private float spc_timerChargeArmsBack = 1.5f;
    private float spc_timeArmingBack = 0;
    #endregion

    #region DELEGATE_VARIABLES
    private delegate bool RageModeDelegate();
    RageModeDelegate rageModeDelegate;

    private delegate bool AttackDelegate();
    AttackDelegate attackDelegate;

    private delegate bool DefenseDelegate();
    DefenseDelegate defenseDelegate;

    private delegate bool NeutralDelegate();
    NeutralDelegate neutralDelegate;

    private bool delegateChanged = false;
    #endregion

    #region RAGE MODE VARIABLES
    public float hipKneeYDistance = 0.2f;
    private float stampChargeTimer = 0;
    private int stampCharge = 0;
    public float stampChargeIncreaseRate = 0.2f;
    public int maxStampCharge = 50;
    public int rageShotMinSpeed = 4;
    private Vector3 oldRageModeShootHandR;
    private Vector3 oldRageModeShootHandL;
    bool bStampAvailable = false;
    #endregion

    #region PICTURES
    public ColourImage kinectCamera;
    private bool pictureArmsBackTaken = false;

    private bool funnyPicture1Taken = false;
    private bool funnyPicture2Taken = false;
    #endregion 
    #endregion

    void Start()
    {
        playerIndex = GetComponent<ScriptPlayer>().playerIndex;
        scriptPlayer = GetComponent<ScriptPlayer>();

        #region DELEGATES
        attackDelegate = checkAttacks;
        defenseDelegate = checkDefenses;
        rageModeDelegate = checkRage;
        #endregion

        #region SPECIAL_ATTACK
        specialMoveCombo[(int)SpecialAttackPoses.crossArms] = false;
        specialMoveCombo[(int)SpecialAttackPoses.armsBehindBack] = false;
        specialMoveCombo[(int)SpecialAttackPoses.armsForward] = false;
        #endregion

        #region SET_PREFERENCES
        switch (stance)
        {
            case DefenseStance.RightFootFront:
                frontFoot = JointType.KneeRight;
                backFoot = JointType.KneeLeft;
                break;
            case DefenseStance.LeftFootFront:
                frontFoot = JointType.KneeLeft;
                backFoot = JointType.KneeRight;
                break;
            default:
                break;
        }

        if (shieldArmChoice == HandChoices.Left)
        {
            shieldingShoulder = JointType.ShoulderLeft;
            shieldElbow = JointType.ElbowLeft;
            shieldArmElbow = BodyAngle.elbowLeft;
            shieldArmPit = BodyAngle.armpitLeft;
            defenseChargeHand = JointType.WristRight;
        }
        else
        {
            shieldingShoulder = JointType.ShoulderRight;
            shieldElbow = JointType.ElbowRight;
            shieldArmElbow = BodyAngle.elbowRight;
            shieldArmPit = BodyAngle.armpitRight;
            defenseChargeHand = JointType.WristLeft;
        }

        if (aimingHandChoice == HandChoices.Left)
        {
            aimingHand = JointType.WristLeft;
            shootingHand = JointType.WristRight;
            //shootingHand = JointType.HandRight;
            aimingArmElbow = JointType.ElbowLeft;
        }
        else
        {
            aimingHand = JointType.WristRight;
            shootingHand = JointType.WristLeft;
            aimingArmElbow = JointType.ElbowRight;
        }

        SendMessage("setShootingHand", shootingHand);
        SendMessage("setAimingHand", aimingHand);
        #endregion

        minZDistance *= -1;
    }

    void Update()
    {
        if (StaticVariables.matchStarted)
        {
            if (skeleton.skeletonAvailable)
            {
                // don't check gestures when the pictures are being shown
                if (!ScriptVsManager.showStats)
                {
                    #region CHECK_GESTURES
                    switch (attackMode)
                    {
                        case AttackMode.attack:
                            checkAttacks();
                            break;

                        case AttackMode.rage:
                            break;

                        default:
                            break;
                    }
                    #endregion

                    withinKinectBounds();

                    checkFunnyPictures();

                    if (zDistance)
                    {
                        if (!moveBack.active)
                            moveBack.SetActiveRecursively(true);
                    }
                    else
                    {
                        if (moveBack.active)
                            moveBack.SetActiveRecursively(false);
                    }
                }


                if (StaticVariables.winnerFound)
                {
                   // if (ScriptVsManager.winnerScreenIsHalfScreen)
                  //  {
                        if (moveBack.active)
                            moveBack.SetActiveRecursively(false);
                  //  }
                }

            
            }
        }
        else
        {
            resetCombo();
        }
    }

    //void OnGUI()
    //{
    //    GUIStyle g = new GUIStyle();
    //    g.fontSize = 60;

    //    if (playerIndex == PlayerIndex.player1)
    //        GUI.Label(new Rect(10, 100, 500, 300), jointPosition(JointType.HandRight).x.ToString(), g);
    //}

    public void checkFunnyPictures()
    {
        if (!funnyPicture1Taken && crossElbows())
        {
            kinectCamera.TakePicture();
            funnyPicture1Taken = true;
        }
        if (!funnyPicture2Taken && ElbowBelowKnee())
        {
            kinectCamera.TakePicture();
            funnyPicture2Taken = true;
        }
    }

    public bool isAiming()
    {
        bool aim = false;

        if (!aimStop && jointPosition(aimingHand).z  > jointPosition(JointType.HipLeft).z/*- jointPosition(JointType.HipCenter).z > aimArmDistance
            && jointPosition(aimingHand).x < jointPosition(shootingHand).x*/)
        {
            aim = true;

            scriptPlayer.playerAiming();

            if (!scriptPlayer.changeToSpecialCam)
            {
                #region GAHTER_ENERGY / NEXT_LEVEL
               /* if (gatheringBallEnergy())
                {
                    scriptPlayer.MagicBallGatheringEnergy(true);
                    if (be_timeGathereingEnergy > be_timeNextLevel)
                    {
                        scriptPlayer.MagicBallNextLevel();
                        be_timeGathereingEnergy = 0;
                    }
                }
                else
                {
                    scriptPlayer.MagicBallGatheringEnergy(false);
                }*/
				isShootNextLevel();

                #endregion

                isShooting();
            }
        }
        else
        {
            aim = false;
            scriptPlayer.playerNotAiming();

            #region RESTORE_VALUES_OF_FUNCTIONS_CALLED_BY_THIS_ONE
            // ensure next time you aim is possible to shoot
            sht_Available = true;
            sht_GestureStarted = false;

            // ball gathering energy restoration
            be_timeGathereingEnergy = 0;
            #endregion
        }

        aiming = aim;
        return aim;
    }

    private bool isShooting()
    {
        bool isShooting = false;

        Vector3 posShootingHand = jointPosition(shootingHand);
        float velocityShootingHand = jointVelocity(shootingHand).z;

        #region DETECT_INIT_GESTURE
        if (sht_Available && !sht_GestureStarted && velocityShootingHand > sht_MinSpeedStart)
        {
            sht_GestureStarted = true;
            sht_initPos = posShootingHand;
            sht_GreatestPos = posShootingHand;
            sht_TimePassed = 0;
        }
        #endregion

        #region CHECK_CANCEL_POSIBILITES
        if (sht_GestureStarted)
        {
            sht_TimePassed += Time.deltaTime;

            // set farthest position of the hand so far.
            if (posShootingHand.z > sht_GreatestPos.z)
            {
                sht_GreatestPos = posShootingHand;
            }

            // check if gesture was canceled (pulling back your shooting hand)
            else if (posShootingHand.z - jointPositionOld(shootingHand).z < sht_Threshold)
            {
                sht_GestureStarted = false;
            }
        }
        #endregion

        #region CHECK_SHOOT
        // shoot if shoot distance covered is in the range of time given
        if (sht_GestureStarted && sht_TimePassed < sht_Time)
        {
            //  Shoot if distance coverd greater than minDistance required and shooting hand beyond your hip
            //if (sht_GreatestPos.z - sht_initPos.z > sht_Distance && posShootingHand.z > jointPosition(JointType.HipCenter).z)
            if (Vector3.Distance(sht_GreatestPos,sht_initPos) > sht_Distance && posShootingHand.z > jointPosition(JointType.HipCenter).z)
            {
                isShooting = true;
                sht_GestureStarted = false;
                sht_Available = false;
                sht_HandCameBack = false;
                sht_TimeWaiting = 0;

                sht_endPos = posShootingHand;

                //Direction shootDirection = checkDirectionShoot(sht_initPos, sht_endPos);
                float speed = (posShootingHand.z - sht_initPos.z) / sht_TimePassed;
                scriptPlayer.AttackProjectile(speed);
                //scriptPlayer.AttackProjectile(speed, shootDirection);

                // reset magic ball power
                chr_Power = 0;
                chr_timeDecreasingPower = 0;
            }
        }
        else
        {
            sht_GestureStarted = false;
        }
        #endregion

        #region CHECK_SHOOTING_AVAILABILITY
        // check that shooting hand came back before shoot again
        if (!sht_HandCameBack)
        {
            if (sht_GreatestPos.z - posShootingHand.z >= sht_DistanceComeBack)
            {
                sht_HandCameBack = true;
            }
            else if (velocityShootingHand < -sht_ComeBackSpeed)
            {
                sht_HandCameBack = true;
            }
        
            // check direction stuff
            sht_timerAfterShoot += Time.deltaTime;
            if (!posDirectionGot && (sht_timerAfterShoot > sht_timeCheckDirectionAfterShoot || sht_HandCameBack))
            {
                posDirectionGot = true;
                sht_posDirection = posShootingHand;
                Direction shootDirection = checkDirectionShoot(sht_initPos, posShootingHand);
            }
        }

        // wait small amount of time before allow shooting again
        if (!sht_Available)
        {
            sht_TimeWaiting += Time.deltaTime;
            if (sht_HandCameBack)
            {
                if (sht_TimeWaiting > sht_TimeNext)
                {
                    sht_Available = true;
                    sht_GestureStarted = false;

                    // check direction stuff
                    sht_timerAfterShoot = 0;
                    posDirectionGot = false;
                }
            }
        }
        #endregion

        return isShooting;
    }

    private Direction checkDirectionShoot(Vector3 initPos, Vector3 finalPos)
    {
        Direction directionShoot = Direction.forward;

        Vector3 endDirection = finalPos - initPos;
        float angle = Vector3.Angle(Vector3.forward, endDirection);

        float dotSide = Vector3.Dot(endDirection, Vector3.right);
        float dotUp = Vector3.Dot(endDirection, Vector3.up);

        #region DECIDE_DIRECTION
        if (angle < sht_angleChangeDirection)
        {
            directionShoot = Direction.forward;
        }
        else if (dotSide > Mathf.Abs(dotUp))
        {
            directionShoot = Direction.right;
        }
        else if (dotUp >= Mathf.Abs(dotSide))
        {
            directionShoot = Direction.up;
        }
        else if (dotSide < dotUp)
        {
            directionShoot = Direction.left;
        }
        else
        {
            directionShoot = Direction.down;
        }
        #endregion

        ////print("Direction Shoot: " + directionShoot.ToString() + "  Angle: " + angle.ToString() + "  DotSide: " + dotSide.ToString() + "  DotUp: " + dotUp.ToString());

        //scriptPlayer.setDirectionBallShooted(directionShoot);
        return directionShoot;
    }

    private bool gatheringBallEnergy()
    {
       be_gatheringEnergy = false;

        if (jointPosition(JointType.WristRight).z < jointPosition(JointType.ShoulderCenter).z)// || jointState(JointType.ShoulderRight) == JointTrackingState.Inferred)
        {
            be_gatheringEnergy = true;
            be_timeGathereingEnergy += Time.deltaTime;
            scriptPlayer.chargingShot(true);
        }
        else
        {
            be_timeGathereingEnergy = 0;
            scriptPlayer.chargingShot(false);
        }

        //if (be_gatheringEnergy)
        //{
        //    be_timeGathereingEnergy += Time.deltaTime;
        //}

        return be_gatheringEnergy;
    }

    public bool checkShield()
    {
        bool shieldActivated = false;

        // don't charge shield if it is activated
        if (!ms_activated)
        {
            if (!ms_waitingForActivation)
            {
                isMagicShieldCharging();
            }

            if (!ms_charging)
            {
                if (isMagicShieldActivated())
                {
                    shieldActivated = true;
                }
            }
        }
        else
        {
            // desactivate shield after charge time consumed
            ms_timeActivated += Time.deltaTime;
            if (ms_timeActivated > ms_timeCharging)
            {
                ms_activated = false;
                scriptPlayer.magicShieldActivated(false);
            }
        }

        return shieldActivated;
    }

    //public bool checkShieldNew()
    //{
    //    bool shieldActive = false;
    //    //if(jointPosition(JointType.HandLeft)


    //    if ((Mathf.Abs(jointPosition(JointType.ElbowLeft).x - jointPosition(JointType.ElbowRight).x) < 0.4f
    //        && Mathf.Abs(jointPosition(JointType.WristLeft).x - jointPosition(JointType.WristRight).x) < 0.4f)
    //        && (jointPosition(JointType.WristLeft).z > jointPosition(JointType.HipCenter).z && jointPosition(JointType.WristRight).z > jointPosition(JointType.HipCenter).z))
    //    {
    //        shieldActive = true;
    //        scriptPlayer.createShield(true);
    //    }
    //    else
    //    {
    //        shieldActive = false;
    //        scriptPlayer.createShield(false);
    //    }


    //    return shieldActive;
    //}

    private bool isMagicShieldCharging()
    {
        bool magicShieldCharging = false;

        // hands up of the head
        if (jointPosition(JointType.HandRight).y > jointPosition(JointType.Head).y
            && jointPosition(JointType.HandLeft).y > jointPosition(JointType.Head).y)
        {
            // start charging counter from zero each time you raise your hands
            if (!ms_charging)
            {
                ms_timeCharging = 0;
            }

            ms_timeCharging += Time.deltaTime;
            ms_timeWaitingActivation = 0;
            magicShieldCharging = true;

            scriptPlayer.magicShieldCharging();

            aimStop = true; // Do not aim while charging
        }
        else
        {
            // if charging shield gesture was done, wait for the activation gesture
            if (ms_charging)
            {
                ms_waitingForActivation = true;
                // have the shield 1 second as minimum
                if (ms_timeCharging * ms_multiplier < 1)
                {
                    ms_timeCharging = 1;
                }
                else
                {
                    ms_timeCharging *= ms_multiplier;
                }
            }

            magicShieldCharging = false;
            aimStop = false;
        }

        ms_charging = magicShieldCharging;
        return magicShieldCharging;
    }

    private bool isMagicShieldActivated()
    {
        bool magicShieldActivated = false;

        if (!ms_charging)
        {
            // hands in front
            if (ms_waitingForActivation
                //&& (jointPosition(JointType.WristRight).z - jointPosition(JointType.HipCenter).z) > ms_forwardDistance
                //&& (jointPosition(JointType.WristLeft).z - jointPosition(JointType.HipCenter).z) > ms_forwardDistance
                //&& jointPosition(JointType.WristRight).y > jointPosition(JointType.ShoulderRight).y
                //&& jointPosition(JointType.WristLeft).y > jointPosition(JointType.ShoulderLeft).y
                )
            {
                magicShieldActivated = true;
                ms_waitingForActivation = false;
                scriptPlayer.magicShieldActivated(true);
                ms_timeActivated = 0;
            }
            else
            {
                // if shield not activated after charging, disable the option of activate it unlees you charge again
                ms_timeWaitingActivation += Time.deltaTime;
                if (ms_timeWaitingActivation > ms_timeToActivate)
                {
                    ms_waitingForActivation = false;
                }
            }
        }

        ms_activated = magicShieldActivated;
        return magicShieldActivated;
    }

    public bool checkAttacks()
    {
       // checkShield();
        // checkShieldNew();

        isAiming();

        isSpecialAttack();

        return true;
    }

    public bool checkRage()
    {
        isRageShooting();
        stampGround();
        return false;
    }

    public void withinKinectBounds()
    {
        //since the kinect data is flipped in the Z, the further from the kinect we move, the number decreases
        if (jointPosition(JointType.HipCenter).z <= minZDistance)
        {
            zDistance = false;
        }
        else
        {
            zDistance = true;
        }

      


    }

    #region FUNNY_GESTURES
    public bool crossElbows()
    {
        if (jointPosition(JointType.ElbowRight).x < jointPosition(JointType.ElbowLeft).x)
        {
            return true;
        }

        return false;
    }

    public bool ElbowBelowKnee()
    {
        if (jointPosition(JointType.ElbowRight).x < jointPosition(JointType.KneeRight).x
            || jointPosition(JointType.ElbowLeft).x < jointPosition(JointType.KneeLeft).x)
        {
            return true;
        }

        return false;
    }
    #endregion

    #region SPECIAL_ATTACK_FUNCTIONS
    public bool isSpecialAttack()
    {
        if (scriptPlayer.specialFilled)
        {
            // if (!aiming)
            //{
            //if (spc_gestureTimer < spc_timeout)
            //{
            /*  if (!specialMoveCombo[(int)SpecialAttackPoses.crossArms])
              {
                  if (isArmsCrossed())
                  {
                      spc_gestureTimer = 0;
                  }
              }
              else*/
            if (/*specialMoveCombo[(int)SpecialAttackPoses.crossArms] &&*/ !specialMoveCombo[(int)SpecialAttackPoses.armsBehindBack])
            {
                if (isArmsBack())
                {
                    spc_gestureTimer = 0;
                    spc_timeArmingBack += Time.deltaTime;
                }
                else
                {
                    if (scriptPlayer.changeToSpecialCam)
                    {
                        scriptPlayer.notChargingSpecial();
                        scriptPlayer.changeToSpecialCam = false;

                    }
                    spc_timeArmingBack = 0;
                }
            }
            else if (specialMoveCombo[(int)SpecialAttackPoses.armsBehindBack] && !specialMoveCombo[(int)SpecialAttackPoses.armsForward])
            {

                if (isArmsForward())
                {
                    spc_gestureTimer = 0;
                }

                //spc_timeArmingBack = 0;
            }
            //}
            //else
            //{
            //    resetCombo();
            //}

            //if (spc_startTimer)
            //{
            //    spc_gestureTimer += Time.deltaTime;
            //}
            // }
        }
        return true;
    }

    private bool isArmsCrossed()
    {
        Vector3 hipCenter = jointPosition(JointType.HipCenter);
        Vector3 leftElbow = jointPosition(JointType.ElbowLeft);
        Vector3 rightElbow = jointPosition(JointType.ElbowRight);
        Vector3 rightWrist = jointPosition(JointType.WristRight);
        Vector3 leftWrist = jointPosition(JointType.WristLeft);
        Vector3 shoulderCenter = jointPosition(JointType.ShoulderCenter);

        if ((rightElbow.z - shoulderCenter.z > spc_armsCrossedDist
            && leftElbow.z - shoulderCenter.z > spc_armsCrossedDist)
            && (rightWrist.y > rightElbow.y && leftWrist.y > leftElbow.y)
            && (leftWrist.x > rightWrist.x))
        {
            specialMoveCombo[(int)SpecialAttackPoses.crossArms] = true;
            spc_startTimer = true;
            return true;
        }
        return false;
    }
    private bool isArmsBack()
    {
       // if ((jointPosition(JointType.HipCenter).z - jointPosition(JointType.ElbowLeft).z > spc_armsBackDist)
        //   && (jointPosition(JointType.HipCenter).z - jointPosition(JointType.ElbowRight).z > spc_armsBackDist))
        if ((jointPosition(JointType.ShoulderCenter).z - jointPosition(JointType.WristLeft).z > spc_armsBackDist)
          && (jointPosition(JointType.ShoulderCenter).z - jointPosition(JointType.WristRight).z > spc_armsBackDist))
        {
            if (spc_timeArmingBack > spc_timerChargeArmsBack)
            {


                scriptPlayer.specialCharged();
                specialMoveCombo[(int)SpecialAttackPoses.armsBehindBack] = true;


            }
           if(spc_timeArmingBack > spc_timerChargeArmsBack-0.5f)
            {
                if (!pictureArmsBackTaken)
                {
                    kinectCamera.TakePicture();
                    pictureArmsBackTaken = true;
                }
            }

            spc_startTimer = true;
            scriptPlayer.ArmsBack();

            return true;
        }
        return false;
    }
    private bool isArmsForward()
    {
        if (jointPosition(JointType.WristLeft).z - jointPosition(JointType.HipCenter).z > spc_forwardDist
          && jointPosition(JointType.WristRight).z - jointPosition(JointType.HipCenter).z > spc_forwardDist)
        {
           
            specialMoveCombo[(int)SpecialAttackPoses.armsForward] = true;
            scriptPlayer.specialAttack();
            resetCombo();
            return true;
        }
        return false;
    }

    private void resetCombo()
    {
        for (int i = 0; i < specialMoveCombo.Length; i++)
        {
            specialMoveCombo[i] = false;
        }
        spc_gestureTimer = 0;
        spc_timeArmingBack = 0;

        spc_startTimer = false;
    }
    #endregion

    public void ResetAfterMatch()
    {
        pictureArmsBackTaken = false;
        funnyPicture1Taken = false;
        funnyPicture2Taken = false;
    }

    #region RAGE_MODE_FUNCTIONS
    public bool stampGround()
    {
        float yDistanceR = jointPosition(JointType.HipRight).y - jointPosition(JointType.KneeRight).y;
        //float yDistanceL = jointPosition(JointType.HipLeft).y - jointPosition(JointType.KneeLeft).y;
        if (!bStampAvailable)
        {
            if (yDistanceR < hipKneeYDistance)// || yDistanceL < hipKneeYDistance)
            {
                // //print("RAISED UP");
                bStampAvailable = true;
            }
        }

        if (bStampAvailable)
        {
            stampChargeTimer += Time.deltaTime;
            if (stampChargeTimer >= 0.1f)
            {
                stampChargeTimer = 0;
                stampCharge++;
                if (stampCharge >= maxStampCharge)
                {
                    stampCharge = maxStampCharge;
                }
            }
            if (yDistanceR > hipKneeYDistance)// || yDistanceL > hipKneeYDistance)
            {

                scriptPlayer.Stamp(stampCharge);
                stampCharge = 0;
                bStampAvailable = false;
            }

        }
        return true;
    }

    public bool isRageShooting()
    {
        if (jointVelocity(JointType.WristLeft).z > rageShotMinSpeed)
        {
            scriptPlayer.RageProjectile(JointType.WristLeft);
        }
        if (jointVelocity(JointType.WristRight).z > rageShotMinSpeed)
        {
            scriptPlayer.RageProjectile(JointType.WristRight);
        }
        return true;
    }
    #endregion

    #region GET_BODY_INFO
    public Quaternion hierJointOrientationQ(JointType type)
    {
        Vector4 tmp = skeleton.boneOrientations[(int)playerIndex][(int)type].hierarchicalRotation.rotationQuaternion;
        return new Quaternion(tmp.x, tmp.y, tmp.z, tmp.w);
    }
    public Quaternion absJointOrientationQ(JointType type)
    {
        Vector4 tmp = skeleton.boneOrientations[(int)playerIndex][(int)type].absoluteRotation.rotationQuaternion;
        return new Quaternion(tmp.x, tmp.y, tmp.z, tmp.w);
    }
    public Matrix4x4 hierJointOrientationM(JointType type)
    {
        return skeleton.boneOrientations[(int)playerIndex][(int)type].hierarchicalRotation.rotationMatrix;
    }
    public Matrix4x4 absJointOrientationM(JointType type)
    {
        return skeleton.boneOrientations[(int)playerIndex][(int)type].absoluteRotation.rotationMatrix;
    }

    public Vector3 jointPosition(JointType type)
    {
        return skeleton.jointPositions[(int)playerIndex, (int)type];
    }
    public Vector3 jointPositionOld(JointType type)
    {
        return skeleton.jointPositionsOld[(int)playerIndex, (int)type];
    }
    public Vector3 jointVelocity(JointType type)
    {
        return skeleton.jointVelocities[(int)playerIndex][(int)type];
    }
    public Vector3 bodyVector(BodyVector vect)
    {
        return skeleton.bodyVectors[(int)playerIndex, (int)vect];
    }
    public float bodyAngle(BodyAngle bodyAng)
    {
        return skeleton.bodyAngles[(int)playerIndex, (int)bodyAng];
    }
    public JointTrackingState jointState(JointType type)
    {
        return skeleton.jointTrackingStates[(int)playerIndex,(int) type];
    }

    float dotProductAng(JointType startJoint1, JointType endJoint1, JointType startJoint2, JointType endJoint2)
    {

        return Mathf.Acos(Vector3.Dot(vectorBetweenJoints(startJoint1, endJoint1), vectorBetweenJoints(startJoint2, endJoint2))) * Mathf.Rad2Deg;
    }

    float dotProductAng(Vector3 v1, Vector3 v2)
    {
        return Mathf.Acos(Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
    }

    Vector3 vectorBetweenJoints(JointType startJoint1, JointType endJoint1)
    {
        return Vector3.Normalize(skeleton.jointPositions[(int)playerIndex, (int)endJoint1] - skeleton.jointPositions[(int)playerIndex, (int)startJoint1]);
    }
    #endregion

    #region OBSOLETE_FUNCTIONS
    public bool checkShakingHand()
    {
        bool shakingHand = false;

        // check for charging if not shielding
        if (attackMode == AttackMode.attack
            || (attackMode == AttackMode.defense && !shieldGestureDone))
        {
            if (isCharged(shootingHand))
            {
                // increase amount you will get next time
                chr_amountRecharge += chr_amountIncreaseByShake;
                if (chr_amountRecharge > chr_maxAmountRecharge)
                {
                    chr_amountRecharge = chr_maxAmountRecharge;
                }

                shakingHand = true;
            }
        }
        else
        {
            chr_playerIsCharging = false;
            chr_numberShakes = 0;
            chr_amountRecharge = chr_initAmountRecharge;
        }

        // communicate the player the changes on the charge
        if (chr_prevPower != chr_Power)
        {
            SendMessage("changeEnergy", chr_Power - chr_prevPower);
        }

        return shakingHand;
    }
    private bool isCharged(JointType hand)
    {
        // Increase power by moving your hand up and down
        chr_prevPower = chr_Power;

        bool chr = false;

        float speed = Mathf.Abs(jointVelocity(hand).y);
        float velocityY = jointVelocity(hand).y;

        chr_timeChargingNext += Time.deltaTime;

        // reset charge values after certain amount of time without doing recharge gesture
        if (chr_playerIsCharging && chr_timeChargingNext > chr_timeResetChargeTimer)
        {
            chr_playerIsCharging = false;
            chr_numberShakes = 0;
            chr_amountRecharge = chr_initAmountRecharge;
        }
        else
        {
            chr_playerIsCharging = true;
            chr_timeChargingNext += Time.deltaTime;
        }


        bool isShakingUp = (velocityY >= 0) ? true : false;
        // First charge can be movement either up or down
        if (chr_playerIsCharging == false && speed > chr_minShakeSpeed)
        {
            chr_Power += chr_amountRecharge;
            chr_upShake = (speed >= 0) ? true : false;
            chr_numberShakes++;
            chr_timeChargingNext = 0;
            chr = true;
        }
        // To keep charging the movement must be opposite to the previous one     
        else if (isShakingUp != chr_upShake && speed > chr_minShakeSpeed)
        {
            chr_Power += chr_amountRecharge;
            chr_upShake = isShakingUp;
            chr_numberShakes++;
            chr_timeChargingNext = 0;
            chr = true;
        }

        return chr;
    }

    public bool switchMagic()
    {
        if ((jointVelocity(aimingHand).z < -switchMagicSpeed) && (jointPositionOld(aimingHand).z >= jointPositionOld(JointType.HipCenter).z)
                 && (jointPosition(aimingHand).z < jointPosition(JointType.HipCenter).z))
        {
            scriptPlayer.SwitchMagicPlayer();

            return true;
        }
        else
        {
            return false;
        }
    }

    public bool isSimpleShield()
    {
        bool shieldGesture = false;
        if (!attackModeChanged && (bodyAngle(shieldArmElbow) < shieldMaxAngleElbow)
            && bodyAngle(shieldArmPit) > shieldMinAngleArmpit
            && jointPosition(shieldElbow).z > jointPosition(shieldingShoulder).z && chr_Power > 0)
        {
            shieldGesture = true;
            scriptPlayer.simpleShieldDefense();
            if (defenseDelegate != isSimpleShield)
            {
                defenseDelegate = isSimpleShield;
            }
        }
        else
        {
            defenseDelegate = checkDefenses;
            scriptPlayer.ShieldDown();

            chr_timeDecreasingPower = 0;
        }

        #region DECREASE_CHARGE_POWER
        if (shieldGesture)
        {
            // Decrease charge power
            chr_timeDecreasingPower += Time.deltaTime;
            if (chr_timeDecreasingPower > chr_timePowerDown)
            {
                chr_Power -= chr_amountRecharge;
                chr_timeDecreasingPower -= chr_timePowerDown;
            }
        }
        #endregion

        shieldGestureDone = shieldGesture;
        return shieldGesture;
    }

    public void activityLevel()
    {
        //int length = skeleton.jointVelocities[(int)playerIndex].Length;
        //Vector3 sumOfSpeeds = Vector3.zero;
        //for (int i = 0; i < length; i++)
        //{
        //    if (skeleton.jointTrackingStates[(int)playerIndex, i] != JointTrackingState.Inferred)
        //    {
        //        sumOfSpeeds += jointVelocity((JointType)i);
        //    }
        //}

        //if (sumOfSpeeds.magnitude > minActiveSpeed)
        //{
        //    scriptPlayer.increaseSpecialBar(sumOfSpeeds.magnitude);
        //}
        ////if (averageSpeed.magnitude > minActiveSpeed)
        ////{
        ////    SendMessage("increaseSpecialBar",averageSpeed);
        ////}

    }

    public AttackMode checkAttackMode()
    {
        attackModeOld = attackMode;

        AttackMode mode;
        if (jointPosition(frontFoot).z - jointPosition(backFoot).z > distanceDefenseMode)
        {
            mode = AttackMode.defense;
        }
        else
        {
            mode = AttackMode.attack;
        }

        scriptPlayer.ChangeMode(mode);

        #region RESET_VALUES_PREVIOUS_ATTACK_MODE
        /* if (attackModeOld != attackMode)
         {
             attackModeChanged = true;
             if (attackModeOld == AttackMode.attack)
             {
                 attackDelegate();
             }
             else if (attackModeOld == AttackMode.defense)
             {
                 defenseDelegate();
             }
             else
             {
                 neutralDelegate();
             }
         }
         else
         {
             attackModeChanged = false;
         }*/
        #endregion

        return mode;
    }

    public bool checkDefenses()
    {
        return true;
    }

    public bool isShootNextLevel()
    {
        //Rise your shooting hand above your head

        bool nextLevel = false;
        if (sht_nextLevelAvailable && jointPosition(shootingHand).y > jointPosition(JointType.Head).y
            && jointVelocity(shootingHand).y > sht_minSpeedLevelUp)
        {
            nextLevel = true;
            scriptPlayer.MagicBallNextLevel();
            sht_nextLevelAvailable = false;
        }

        if (!sht_nextLevelAvailable && jointPosition(shootingHand).y < jointPosition(JointType.Head).y)
        {
            sht_nextLevelAvailable = true;
        }

        return nextLevel;
    }
    #endregion
}
