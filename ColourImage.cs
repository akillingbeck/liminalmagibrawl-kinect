﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.IO;
using System.Security.AccessControl;

public class ColourImage : MonoBehaviour
{

    //private string fileName = "C:\\Users\\a204096\\Desktop\\ScreenShots\\Liminal_Magibrawl";
    private string fileName = "C:\\temp";
    public TextureFormat format;
    private TextureFormat oldFormat;
    private KinectDevice kinect;
    [HideInInspector]
    public static bool done = true;
    private Vector2 scale;
    public int maxPictures = 10;
    [HideInInspector]
    public int picturesTaken = 0;
    private static bool showPictures = false;
    [HideInInspector]
    public List<Texture2D> texToDisplay;
    void Awake()
    {
        texToDisplay = new List<Texture2D>();
        //texToDisplay = new Texture2D[maxPictures];
        scale = new Vector2(-1f, 1f);
        kinect = gameObject.GetComponent<KinectDevice>();

        oldFormat = format;
    }

    //void OnGUI()
    //{
    //    if (showPictures)
    //    {
    //        if (Event.current.type == EventType.Repaint)
    //        {
    //            GUIUtility.ScaleAroundPivot(scale, new Vector2(160, 120));
    //            GUIUtility.RotateAroundPivot(180, new Vector2(160, 120));

    //            if (texToDisplay[0])
    //                GUI.DrawTexture(new Rect(0, 0, 320, 240), texToDisplay[0], ScaleMode.StretchToFill);
    //            if (texToDisplay[1])
    //                GUI.DrawTexture(new Rect(320, 0, 320, 240), texToDisplay[1], ScaleMode.StretchToFill);
    //            if (texToDisplay[2])
    //                GUI.DrawTexture(new Rect(640, 0, 320, 240), texToDisplay[2], ScaleMode.StretchToFill);
    //        }
    //    }
    //}

    public void TakePicture()
    {
        if (kinect.PollColourData())
        {
            if (picturesTaken < maxPictures)
            {
                Texture2D tempTex = new Texture2D(640, 480, format, false);
                texToDisplay.Add(tempTex);
                texToDisplay[picturesTaken].SetPixels32(mipmapImg(kinect.colorImage, 1280, 960));
                //mipmapImg(kinect.colorImage,640,480);
                texToDisplay[picturesTaken].Apply(false);

                picturesTaken++;
            }
            done = true;
        }
    }

    void Update()
    {
        //    if(Input.GetKeyDown(KeyCode.Space))
        //    {
        //        done = false;
        //        //print("PICTURE TAKEN");
        //    }


        //if (Input.GetKeyDown(KeyCode.X))
        //{
        //    showPictures = !showPictures;
        //    savePicturesToFile();
        //}

        //if(!done)
        //    {
        //    if (kinect.PollColourData())
        //    {

        //        if(picIndex < maxPictures)
        //        {
        //            //print("INDEX "+picIndex);
        //        texToDisplay[picIndex].SetPixels32(mipmapImg(kinect.colorImage, 1280, 960));
        //        //mipmapImg(kinect.colorImage,640,480);
        //        texToDisplay[picIndex].Apply(false);

        //        picIndex++;

        //        }
        //        else
        //        {
        //            picIndex = 0;
        //        }
        //        done = true;
        //    }

        //}
        // }
    }

    public List<Texture2D> getImages()
    {
        return texToDisplay;
    }

    /*void savePicturesToFile()
    {
        int picTure = 0;

        for (int i = 0; i < maxPictures; i++)
        {

            byte[] image = texToDisplay[i].EncodeToPNG();
            fileName += picTure + ".png";

            FileStream fs = File.Open(fileName, FileMode.Create, FileAccess.Write);//Create(fileName,image.Length,

            fs.Write(image, 0, image.Length);

            fs.Close();

            fileName = "C:\\Users\\a204096\\Desktop\\ScreenShots\\Liminal_Magibrawl";

            picTure++;
        }
    }
     * */

    public void Reset()
    {
        done = false;
        picturesTaken = 0;
        texToDisplay.Clear();
    }

    private Color32[] mipmapImg(Color32[] src, int width, int height)
    {
        int newWidth = width / 2;
        int newHeight = height / 2;

        Color32[] dst = new Color32[newWidth * newHeight];
        for (int yy = 0; yy < newHeight; yy++)
        {
            for (int xx = 0; xx < newWidth; xx++)
            {
                int TRidx = (xx * 2 + 1) + yy * width * 2;
                dst[xx + yy * newWidth] = src[TRidx];
            }

        }
        return dst;
    }
}

