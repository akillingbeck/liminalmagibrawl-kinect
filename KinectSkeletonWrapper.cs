using UnityEngine;
using System.Collections;
using KinectWrapper;

/// <summary>
/// akill08 - Wrapper class for holding kinect skeletal data
/// </summary>
[RequireComponent(typeof(KinectDevice))]
public class KinectSkeletonWrapper : MonoBehaviour 
{

    private KinectDevice device; 
    //akill08 - We want quick access to things like joint positions, joint tracking states
    //          - store both players skeletal info in the same 2D array

    [HideInInspector]
    public Vector4 skeletonGeneralPosition;

    [HideInInspector]
    public Vector4 clipPlane;

    //[HideInInspector]
    //public float[,] limbLengths;
    [HideInInspector]
    public Vector3[,] jointPositions;

    [HideInInspector]
    public Vector3[,] jointPositionsOld;

    [HideInInspector]
    public Vector3[][] jointVelocities;

    [HideInInspector]
    public SkeletonBoneOrientation[][] boneOrientations;
	
	// Body vectors
	[HideInInspector]
    public Vector3[,] bodyVectors;

    // Body angles
    [HideInInspector]
    public float[,] bodyAngles;
    
    [HideInInspector]
    public KinectWrapper.JointTrackingState[,] jointTrackingStates;
    [HideInInspector]
    public int[] trackedPlayers; //akill08 - updated each frame to determine which players are tracked
	[HideInInspector]
    public bool skeletonAvailable = false;

    private bool bPolledSkeleton = false;
    private bool bSkelFrameRetrieved = false;
    long currentAngle = 0;
    private Matrix4x4 flip = Matrix4x4.identity;

    // ngs - rotate joint positions data depending on kinect tilt, to match kinect xyz coordinates system
    // with our real xyz coordinates (assuming floor tilt 0 degrees)
    private Matrix4x4 rotate = Matrix4x4.identity; 
                                                        
    void Awake()
    {
       // DontDestroyOnLoad(ga
    }
	// Use this for initialization
	void Start ()
    {
        device = gameObject.GetComponent<KinectDevice>();
        flip[2, 2] = -1;

        // create rotation matrix depending on kinect tilt
        long angle = 0;
        Kinect.SetCameraAngle(8);
        Kinect.GetCameraAngle(ref angle);
        rotate = Matrix4x4.TRS(Vector3.zero, Quaternion.AngleAxis(angle, Vector3.right), Vector3.one);
        flip = rotate * flip;

        trackedPlayers = new int[2];
        trackedPlayers[0] = -1;
        trackedPlayers[1] = -1;

        boneOrientations = new SkeletonBoneOrientation[2][];
        boneOrientations[0] = new SkeletonBoneOrientation[(int)KinectWrapper.JointType.Count];
        boneOrientations[1] = new SkeletonBoneOrientation[(int)KinectWrapper.JointType.Count];

        skeletonGeneralPosition = new Vector4();
        clipPlane = new Vector4();

        jointPositions = new Vector3[2,(int) KinectWrapper.JointType.Count];
        jointPositionsOld = new Vector3[2, (int)KinectWrapper.JointType.Count];

        jointVelocities = new Vector3[2][];
        jointVelocities[0] = new Vector3[(int)KinectWrapper.JointType.Count];
        jointVelocities[1] = new Vector3[(int)KinectWrapper.JointType.Count];

      //  limbLengths = new float[2,(int)Limbs.Count];

		bodyVectors = new Vector3[2, (int)KinectWrapper.BodyVector.Count];
        bodyAngles = new float[2, (int)KinectWrapper.BodyAngle.Count];
		
        jointTrackingStates = new KinectWrapper.JointTrackingState[2, (int)KinectWrapper.JointType.Count];
	
	}


    /// <summary>
    /// akill08 - We only want to reset these flags once everything has finished updating
    ///          LateUpdate() gets called once all updates have been called
    /// </summary>
    void LateUpdate()
    {
        bPolledSkeleton = false;
        bSkelFrameRetrieved = false;
      
    }

    public void flipPosition(int M1,int M2)
    {
        flip[M1, M2] *= -1;
    }
    void Update()
    {

        if (PollSkeleton())
        {
            skeletonAvailable = true;
        }
        else
            skeletonAvailable = false;
    }
    public bool PollSkeleton()
    {
        if(!bPolledSkeleton)
        {
            bPolledSkeleton = true;

            //akill08 - If a skeleton frame was successully polled from the stream, 
            //          we can then poll the skeleton data from that frame
            if(device.PollSkeletonData())
            {
             //   ////print("Frame Received");
                bSkelFrameRetrieved = true;
                ProcessSkeletonData();
            }
        }
        return bSkelFrameRetrieved;
    }


    /// <summary>
    /// akill08 -  Helper fnction to access skeletonData at a particular index
    /// </summary>
    /// <param name="index">The index in the skeleton frames array of skeleton data the tracked skeleton is recorded</param>
    /// <returns>The SkeletonData structure at the position specified</returns>
    public KinectWrapper.SkeletonData SkeletonData(int index)
    {     
        return device.skeletonFrame.skeletonData[index];
    }


    void ProcessSkeletonData()
    {
        #region TRACK_SKELETONS
        //akill08 - We need a variable to store the index of the tracked skeleton data each frame
        //          give initial values of -1
        int[] trackedIndex = new int[KinectWrapper.SkeletonConstants.MaxSkeletonTracked];
        trackedIndex[0] = -1;
        trackedIndex[1] = -1;
        //akill08 - Counter for the skeletons tracked each frame (wont go beyond 2)
        int skeletonsTracked = 0;

        //step 1: Find if a skeleton is being tracked
        //step 2: Store the index of this skeleton
        //step 3: Record how many skeletons are tracked this frame (max 2) and Last frame(gonna need different variables)
        //step 4: Determine which player gets assigned which skeleton index (should be the same as last frame if it was tracked)
        //step 5: Fill jointPosition array with each players data

        for(int i = 0; i < KinectWrapper.SkeletonConstants.SkeletonCount; i++)
        {
            if(SkeletonData(i).trackingState == KinectWrapper.SkeletonTrackingState.Tracked)
            {
                //akill08 - If a skeleton was tracked, we can store its index temporarily
                trackedIndex[skeletonsTracked] = i;
                //akill08 - a new skeleton is tracked!
                skeletonsTracked++;
            }
        }


        //akill08 - We have the number of skeletons tracked this frame
        //          Now we can determine what to do depending on this
        switch(skeletonsTracked)
        {
                //akill08 - 0 skeletons tracked this frame. This means we can reset last frames tracked players, so they no longer
                //          point to skeleton data
            case 0:
                trackedPlayers[0] = -1;
                trackedPlayers[1] = -1;
                break;
                //akill08 - For the 2nd case, there are a few different scenarios we need to check
                //          1 - There were no players tracked last frame, so give the index to any of them(1st index makes most sense)
                //          2 - There was a player last frame, we try to give them the same skeleton index
                //          3 - There were 2 players tracked last frame, so we want to keep the one that still has the same index
                //  Because there will only be 1 player tracked, we know that trackedIndex[0] will be occupied!
            case 1:
                if(trackedPlayers[0] < 0 && trackedPlayers[1] < 0)
                {
                    //akill08 - Give player 1 the first skeleton tracked
                    trackedPlayers[0] = trackedIndex[0];
                }
                    //akill08 - If trackedPlayers[0] wasn't occupied last frame, it means trackedPlayers[1] was!
                else if(trackedPlayers[0] < 0)
                {
                    trackedPlayers[1] = trackedIndex[0];
                }
                //akill08 - If trackedPlayers[1] wasn't occupied last frame, it means trackedPlayers[0] was!
                else if(trackedPlayers[1] < 0)
                {
                    trackedPlayers[0] = trackedIndex[0];
                }
                else
                {
                    //akill08 - Check if trackedPlayers[0] had the same index as the 1 currently tracked skeleton
                    if(trackedIndex[0] == trackedPlayers[0])
                    {
                        //akill08 - If trackedPlayers[0] had the same index as the currently tracked skeleton, we can set the other playerIndex
                        //          back to default. (trackedPlayers[0] is still being tracked)
                        trackedPlayers[1] = -1;
                    }
                    //akill08 - Check if trackedPlayers[1] had the same index as the 1 currently tracked skeleton
                    else if(trackedIndex[0] == trackedPlayers[1])
                    {
                        //akill08 - If trackedPlayers[1] had the same index as the currently tracked skeleton, we can set the other playerIndex
                        //          back to default. (trackedPlayers[1] is still being tracked)
                        trackedPlayers[0] = -1;
                    }
                    else
                    {
                        //akill08 - If there was a problem keeping track of the skeletons, we give the index to the first player
                        trackedPlayers[0] = trackedIndex[0];
                        trackedPlayers[1] = -1;
                    }
                }
                break;
            case 2:
                //akill08 - Again, for 2 tracked players there are some scenarios we need to check
                //          1 - There wasnt any tracked players last frame, so just assign them
                //          2 -  There was 1 player tracked last frame, so we need to give them the same index, and give the new
                //              trackedPlayer the other one
                //          3 - There were 2 players tracked, so we need to try and give them the same index the had
                if(trackedPlayers[0] < 0 && trackedPlayers[1] < 0)
                {
                    trackedPlayers[0] = trackedIndex[0];
                    trackedPlayers[1] = trackedIndex[1];
                }
                 //akill08 - If trackedPlayers[0] wasn't tracked last frame, it means we need to find out which index trackedPlayers[1]
                 //         had
                else if(trackedPlayers[0] < 0)
                {
                    //akill08 - If  trackedPlayers[1] was tracked at the index trackedIndex[0], give trackedPlayers[0] the other spot
                    if(trackedPlayers[1] == trackedIndex[0])
                    {
                        trackedPlayers[0] = trackedIndex[1];
                    }
                        //akill08 - Otherwise do the opposite
                    else
                    {
                        trackedPlayers[0] = trackedIndex[0];
                        trackedPlayers[1] = trackedIndex[1];
                    }
                }
                //akill08 - If trackedPlayers[1] wasn't tracked last frame, it means we need to find out which index trackedPlayers[0]
                //         had
                else if(trackedPlayers[1] < 0)
                {
                    //akill08 - If  trackedPlayers[0] was tracked at the index trackedIndex[1], give trackedPlayers[1] the other spot
                    if(trackedPlayers[0] == trackedIndex[1])
                    {
                        trackedPlayers[1] = trackedIndex[0];
                    }
                    //akill08 - Otherwise do the opposite
                    else
                    {
                        trackedPlayers[0] = trackedIndex[0];
                        trackedPlayers[1] = trackedIndex[1];
                    }
                }
                    //akill08 - There were 2 players tracked, find out which index they held, and keep it that way
                else
                {
                    //akill08 - If either of these are true, then we automatically know the 2nd index (since 2 skeletons were tracked last frame, none of the array entries can equal -1)
                    if(trackedPlayers[0] == trackedIndex[1] || trackedPlayers[1] == trackedIndex[0])
                    {
                        trackedPlayers[0] = trackedIndex[1];
                        trackedPlayers[1] = trackedIndex[0];
                    }
                       
                    else
                    {
                        trackedPlayers[0] = trackedIndex[0];
                        trackedPlayers[1] = trackedIndex[1];
                    }
                }
                break;
        }//end switch block
        #endregion


        //We want the player on the left of the kinect to always be the one who is player 1,
        //Since X = 0 is in the centre of the kinect, we just check who is less than 0 and set them to be player 1
        if (trackedPlayers[0] != -1 && trackedPlayers[1]!=-1)
        {
            KinectWrapper.SkeletonData tmpSkeleton1 = SkeletonData(trackedPlayers[0]);
            KinectWrapper.SkeletonData tmpSkeleton2 = SkeletonData(trackedPlayers[1]);

            if (tmpSkeleton2.skeletonPositions[(int)JointType.HipCenter].x < tmpSkeleton1.skeletonPositions[(int)JointType.HipCenter].x)
            {
                int temp = trackedPlayers[0];
                trackedPlayers[0] = trackedPlayers[1];
                trackedPlayers[1] = temp;
            }
        }
        #region UPDATE_DATA
        //akill08 - Now we need to update the joint positions and joint states into the 2D arrays
        //          We need to perform the loop for each player, storing as [playerIndex,joint]

        for(int playerIndex = 0; playerIndex < KinectWrapper.SkeletonConstants.MaxSkeletonTracked; playerIndex++)
        {        
            //akill08 - Make sure the skeleton is actually pointing to data
            if(trackedPlayers[playerIndex] != -1)
            {
                KinectWrapper.SkeletonData tmpSkeleton = SkeletonData(trackedPlayers[playerIndex]);
                //clipPlane = device.skeletonFrame.ClipPlane;
                //skeletonGeneralPosition = tmpSkeleton.position;

                for(int joint = 0; joint < (int)KinectWrapper.JointType.Count; joint++)
                {
                    jointPositionsOld[playerIndex, joint] = jointPositions[playerIndex, joint];
                    jointPositions[playerIndex, joint] = flip.MultiplyPoint3x4(tmpSkeleton.skeletonPositions[joint]);
                    jointTrackingStates[playerIndex, joint] = tmpSkeleton.jointTrackingState[joint];             
                    // Update velocity of each joint
                    jointVelocities[playerIndex][joint] = (jointPositions[playerIndex, joint] - jointPositionsOld[playerIndex, joint]) / Time.deltaTime;
                }

              //  calculateLimbLengths(playerIndex);
               
                
                Kinect.CalculateBoneOrientations(ref tmpSkeleton, boneOrientations[playerIndex]);
            }
			UpdateBodyVectors(playerIndex);
            UpdateAngles(playerIndex);
        }
        #endregion
    }
	private void UpdateBodyVectors(int playerIndex)
	{    
		bodyVectors[playerIndex, (int)BodyVector.Head_ShoulderCenter] = jointPositions[playerIndex, (int)JointType.ShoulderCenter] - jointPositions[playerIndex, (int)JointType.Head];
		bodyVectors[playerIndex, (int)BodyVector.ShoulderCenter_ShoulderRight] = jointPositions[playerIndex, (int)JointType.ShoulderRight] - jointPositions[playerIndex, (int)JointType.ShoulderCenter];		
		bodyVectors[playerIndex, (int)BodyVector.ShoulderRight_ElbowRight] = jointPositions[playerIndex, (int)JointType.ElbowRight] - jointPositions[playerIndex, (int)JointType.ShoulderRight];
		bodyVectors[playerIndex, (int)BodyVector.ElbowRight_WristRight] = jointPositions[playerIndex, (int)JointType.WristRight] - jointPositions[playerIndex, (int)JointType.ElbowRight];		
		bodyVectors[playerIndex, (int)BodyVector.WristRight_HandRight] = jointPositions[playerIndex, (int)JointType.HandRight] - jointPositions[playerIndex, (int)JointType.WristRight];		
		bodyVectors[playerIndex, (int)BodyVector.ShoulderCenter_ShoulderLeft] = jointPositions[playerIndex, (int)JointType.ShoulderLeft] - jointPositions[playerIndex, (int)JointType.ShoulderCenter];		
		bodyVectors[playerIndex, (int)BodyVector.ShoulderLeft_ElbowLeft] = jointPositions[playerIndex, (int)JointType.ElbowLeft] - jointPositions[playerIndex, (int)JointType.ShoulderLeft];		
		bodyVectors[playerIndex, (int)BodyVector.ElbowLeft_WristLeft] = jointPositions[playerIndex, (int)JointType.WristLeft] - jointPositions[playerIndex, (int)JointType.ElbowLeft];		
		bodyVectors[playerIndex, (int)BodyVector.WristLeft_HandLeft] = jointPositions[playerIndex, (int)JointType.HandLeft] - jointPositions[playerIndex, (int)JointType.WristLeft];		
		bodyVectors[playerIndex, (int)BodyVector.ShoulderCenter_Spine] = jointPositions[playerIndex, (int)JointType.Spine] - jointPositions[playerIndex, (int)JointType.ShoulderCenter];		
		bodyVectors[playerIndex, (int)BodyVector.Spine_HipCenter] = jointPositions[playerIndex, (int)JointType.HipCenter] - jointPositions[playerIndex, (int)JointType.Spine];		
		bodyVectors[playerIndex, (int)BodyVector.HipCenter_HipRight] = jointPositions[playerIndex, (int)JointType.HipRight] - jointPositions[playerIndex, (int)JointType.HipCenter];		
		bodyVectors[playerIndex, (int)BodyVector.HipRight_KneeRight] = jointPositions[playerIndex, (int)JointType.KneeRight] - jointPositions[playerIndex, (int)JointType.HipRight];		
		bodyVectors[playerIndex, (int)BodyVector.KneeRight_AnkleRight] = jointPositions[playerIndex, (int)JointType.AnkleRight] - jointPositions[playerIndex, (int)JointType.KneeRight];		
		bodyVectors[playerIndex, (int)BodyVector.AnkleRight_FootRight] = jointPositions[playerIndex, (int)JointType.FootRight] - jointPositions[playerIndex, (int)JointType.AnkleRight];		
		bodyVectors[playerIndex, (int)BodyVector.HipCenter_HipLeft] = jointPositions[playerIndex, (int)JointType.HipLeft] - jointPositions[playerIndex, (int)JointType.HipCenter];		
		bodyVectors[playerIndex, (int)BodyVector.HipLeft_KneeLeft] = jointPositions[playerIndex, (int)JointType.KneeLeft] - jointPositions[playerIndex, (int)JointType.HipLeft];		
		bodyVectors[playerIndex, (int)BodyVector.KneeLeft_AnkleLeft] = jointPositions[playerIndex, (int)JointType.AnkleLeft] - jointPositions[playerIndex, (int)JointType.KneeLeft];
		bodyVectors[playerIndex, (int)BodyVector.AnkleLeft_FootLeft] = jointPositions[playerIndex, (int)JointType.FootLeft] - jointPositions[playerIndex, (int)JointType.AnkleLeft];
        bodyVectors[playerIndex, (int)BodyVector.ShoulderCenter_HipCenter] = jointPositions[playerIndex, (int)JointType.HipCenter] - jointPositions[playerIndex, (int)JointType.ShoulderCenter];
        bodyVectors[playerIndex, (int)BodyVector.ShoulderLeft_WristLeft] = jointPositions[playerIndex, (int)JointType.WristLeft] - jointPositions[playerIndex, (int)JointType.ShoulderLeft];		
	}

    private void UpdateAngles(int playerIndex)
    {
        // ngs - angle between right arm and right forearm
        bodyAngles[playerIndex, (int)BodyAngle.elbowRight] = Vector3.Angle(-1 * bodyVectors[playerIndex, (int)BodyVector.ShoulderRight_ElbowRight],
                                                                              bodyVectors[playerIndex, (int)BodyVector.ElbowRight_WristRight]);

        //angle between right arm and right forearm
        bodyAngles[playerIndex, (int)BodyAngle.elbowLeft] = Vector3.Angle(-1 * bodyVectors[playerIndex, (int)BodyVector.ShoulderLeft_ElbowLeft],
                                                                                bodyVectors[playerIndex, (int)BodyVector.ElbowLeft_WristLeft]);
        // angle armpit right
        bodyAngles[playerIndex, (int)BodyAngle.armpitRight] = Vector3.Angle(bodyVectors[playerIndex, (int)BodyVector.ShoulderRight_ElbowRight],
                                                                                bodyVectors[playerIndex, (int)BodyVector.ShoulderCenter_HipCenter]);

        // angle armpit left
        bodyAngles[playerIndex, (int)BodyAngle.armpitLeft] = Vector3.Angle(bodyVectors[playerIndex, (int)BodyVector.ShoulderLeft_ElbowLeft],
                                                                                bodyVectors[playerIndex, (int)BodyVector.ShoulderCenter_HipCenter]);

      

    }

    //TODO: Put this in a calibration class or something
    //private void calculateLimbLengths(int playerIndex)
    //{
    //    limbLengths[playerIndex, (int)Limbs.Forearm] =
    //               Mathf.Abs((jointPositions[playerIndex, (int)JointType.WristLeft] - jointPositions[playerIndex, (int)JointType.ElbowLeft]).magnitude);

    //    limbLengths[playerIndex, (int)Limbs.UpperArm] =
    //        Mathf.Abs((jointPositions[playerIndex, (int)JointType.ElbowLeft] - jointPositions[playerIndex, (int)JointType.ShoulderLeft]).magnitude);

    //    limbLengths[playerIndex, (int)Limbs.ShoulderWidth] =
    //       Mathf.Abs((jointPositions[playerIndex, (int)JointType.ShoulderLeft] - jointPositions[playerIndex, (int)JointType.ShoulderRight]).magnitude);

    //    limbLengths[playerIndex, (int)Limbs.Spine] =
    //       Mathf.Abs((jointPositions[playerIndex, (int)JointType.ShoulderCenter] - jointPositions[playerIndex, (int)JointType.HipCenter]).magnitude);

    //    limbLengths[playerIndex, (int)Limbs.UpperLeg] =
    //       Mathf.Abs((jointPositions[playerIndex, (int)JointType.HipLeft] - jointPositions[playerIndex, (int)JointType.KneeLeft]).magnitude);

    //    limbLengths[playerIndex, (int)Limbs.LowerLeg] =
    //       Mathf.Abs((jointPositions[playerIndex, (int)JointType.ElbowLeft] - jointPositions[playerIndex, (int)JointType.AnkleLeft]).magnitude);
    //}
	
	
}
