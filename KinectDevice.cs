using UnityEngine;
using System.Collections;
using System;
using KinectWrapper;
using System.Runtime.InteropServices;
public class KinectDevice : MonoBehaviour {

    public static int count = 0;
    public float smoothing = 0.5f;             // [0..1], lower values closer to raw data
    public float correction = 0.5f;            // [0..1], lower values slower to correct towards the raw data
    public float prediction = 0.5f;            // [0..n], the number of frames to predict into the future
    public float jitterRadius =0.05f;          // The radius in meters for jitter reduction
    public float maxDeviationRadius = 0.04f;    //The maximum radius in meters that filtered positions are allowed to deviate from raw data
  
    [HideInInspector]
    public SkeletonFrame skeletonFrame = new SkeletonFrame()
    {
        //skeletonData = new SkeletonData[SkeletonConstants.SkeletonCount]
    };

     [HideInInspector]
    public TransformSmoothParameters smoothParameters = new TransformSmoothParameters();

     private bool bPolledSkeleton = false;
     private bool bSkelFrameRetrieved = false;

     private bool updatedColor = false;
     private bool newColor = false;

     private IntPtr colorStreamHandle;

    [HideInInspector]
     public Color32[] colorImage;

    

    void Awake()
    {
        ////print("Device start called!");
        count++;
        try
        {
            int result = Kinect.Initialize( KinectInitialiseFlags.UsesColor  | KinectInitialiseFlags.UsesSkeleton);

            if(result != 0)
            {
                throw new Exception("Initialisation failed.");
            }

            result = Kinect.EnableSkeletonTracking(IntPtr.Zero, SkeletonTrackingFlags.SuppressNoFrameData);
            if(result != 0)
            {
                throw new Exception("Cannot initialize Skeleton Data.");
            }

            colorStreamHandle = IntPtr.Zero;
			
			result = Kinect.OpenImageStream(ImageTypeFlags.colour, ImageResolution.res1280x960, 0, 2, IntPtr.Zero, ref colorStreamHandle);
			
            //Debug.Log(colorStreamHandle);
            if (result != 0)
            {
                throw new Exception("Cannot open color stream.");
            }
            colorImage = new Color32[1280 * 960];
			
			
			Kinect.NuiSetDeviceStatusCallback(new NuiStatusProc());
			
            DontDestroyOnLoad(gameObject);
        }
        catch(Exception e)
        {
            Debug.Log(e.Message);
        }

    }
	
	// Update is called once per frame
    /// <summary>
    /// akill08 - We only want to reset these flags once everything has finished updating
    ///          LateUpdate() gets called once all updates have been called
    /// </summary>
	void LateUpdate () 
    {
        bPolledSkeleton = false;
        bSkelFrameRetrieved = false;
        updatedColor = false;
        newColor = false;
	}

    /// <summary>
    /// get status of Kinect skeleton stream
    /// </summary>
    public bool PollSkeletonData()
    {
       
       if(!bPolledSkeleton)
       {
           bPolledSkeleton = true;
           int result = Kinect.GetNextSkeletonFrame(2, ref skeletonFrame);
           //akill08 -  No errors means we have sucessfully retrieved skeleton frame from skeleton stream
           //          We set bSkelFrameRetrieved = true so we can let callers know they can access skeleton data from
           //          the frame without any problems
           if(result == 0)
           {
               bSkelFrameRetrieved = true;
           }
           smoothParameters.Smoothing = smoothing;
           smoothParameters.Correction = correction;
           smoothParameters.JitterRadius = jitterRadius;
           smoothParameters.Prediction = prediction;
           smoothParameters.MaxDeviationRadius = maxDeviationRadius;
           result = Kinect.TransformSmooth(ref skeletonFrame, ref smoothParameters);
	   
       }
        return bSkelFrameRetrieved;
       
    }

    public bool PollColourData()
    {
        if (!updatedColor)
        {
			
			
            updatedColor = true;
            IntPtr imageStreamPtr = IntPtr.Zero;
            int hr= Kinect.GetNextImageFrame(colorStreamHandle,2,ref imageStreamPtr);
			
			
            if (hr == 0)
            {
				ImageFrame imageFrame = (ImageFrame)Marshal.PtrToStructure(imageStreamPtr,typeof(ImageFrame));
				
				ImageBuffer imgBuffer = (ImageBuffer)Marshal.PtrToStructure(imageFrame.ImageFrameTexture,typeof(ImageBuffer));
				
				
				
				
				if(imgBuffer == null)
				{
					int t= 0;
				}
				
				uint width = 0;
				uint height =0;
				ImageResToSize(imageFrame.imageRes,out width,out height);
			
				colorImage = extractColorImage(imgBuffer,width,height);
				
				//if(colorImage.Length > 0)
				//{
                newColor = true;
				//}
				
				hr = Kinect.ReleaseImageFrame(colorStreamHandle, imageStreamPtr);
				if(hr == 0)
				{
					//Debug.Log("Released Frame");
				}
            }
			//else
			//{
			//	hr = Kinect.ReleaseImageFrame(colorStreamHandle, imageStreamPtr);
			//}
            //if (hr == 0)
            //{
            //    //print("HERE");
            //    newColor = true;
            //    ImageFrame imageFrame = (ImageFrame)Marshal.PtrToStructure(imageFramePtr, typeof(ImageFrame));
            //    //print("RES " + imageFrame.imageRes);

            //    //Debug.Log("ptr:" + imageFramePtr + " frame:" + imageFrame.ImageFrameTexture);
                //ImageBuffer imageBuf = (ImageBuffer)Marshal.PtrToStructure(imageFrame.ImageFrameTexture, typeof(ImageBuffer));
            //   // Debug.Log("width:" + imageBuf.m_Width + " height:" + imageBuf.m_Height + " total:" + (imageBuf.m_Width * imageBuf.m_Height * imageBuf.m_BytesPerPixel));
            //    uint width = 0;
            //    uint height = 0;
            //    ImageResToSize(imageFrame.imageRes, out width, out height);
            //    colorImage = extractColorImage(imageBuf,width,height);

            //   // hr = Kinect.ReleaseImageStream(colorStreamHandle, imageFramePtr);
            //    if (hr != 0)
            //    {
            //        Debug.Log("Error releasing frame");
            //    }
            //}
        }
        return newColor;
    }
    void ImageResToSize(ImageResolution res,out uint width,out uint height)
    {
        width = 0;
        height = 0;
        switch (res)
        {
            case ImageResolution.Invalid:
                break;
            case ImageResolution.res80x60:
                width = 80;
                height = 60;
                break;
            case ImageResolution.res320x240:
                 width = 320;
                height = 240;
                break;
            case ImageResolution.res640x480:
                 width = 640;
                height = 480;
                break;
            case ImageResolution.res1280x960:
                 width = 1280;
                height = 960;
                break;
            default:
                width = 0;
                height = 0;
                break;
        }

    }
    private Color32[] extractColorImage(ImageBuffer buf,uint width,uint height)
    {

		
        int totalPixels = (int)(buf.width * buf.height);
        Color32[] colorBuf = colorImage;
        ColorBuffer cb = (ColorBuffer)Marshal.PtrToStructure(buf.pBuffer, typeof(ColorBuffer));
		//byte[] src = buf.src;
		
		int index = 0;
		for(int i=0;i<totalPixels;i++)
		{

			colorBuf[i].r = cb.pixels[i].r;
			colorBuf[i].g = cb.pixels[i].g;
			colorBuf[i].b = cb.pixels[i].b;

			colorBuf[i].a = 255;

		}

      
       return colorBuf;
   }

	void OnApplicationQuit()
	{
        Kinect.Shutdown();
	}
}
